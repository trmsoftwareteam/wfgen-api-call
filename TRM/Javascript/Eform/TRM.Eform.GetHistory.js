﻿// define namespace Antelope.Math under DMSEForm
DMSEForm.lib.namespace("TRM.Eform");

// define the class SumValue in namespace
DMSEForm.TRM.Eform.GetHistory = function () {
    // specify the class name for identify this class
    this.description = 'Get History';
    // specify the trigger logic when event occurs, the form object is the current form, and arg is the value passed depends on the event.
    this.trigger = function (form, arg) {
        var initialTaskWiId = form.getObjectById("Initial task wiId");
        try {
            initialTaskWiId.setData(window.parent.currentWiId);
            initialTaskWiId.setVisible(0);
        } catch (err) { }


        for (var i = 0; i < 11; i++) {
            var sectionObj = null;
            if (i == 0) {
                sectionObj = form.getObjectById("Initial task");
            } else if (i == 10) {
                objId = "Task" + i.toString();
                sectionObj = form.getObjectById(objId);
            } else {
                objId = "Task0" + i.toString();
                sectionObj = form.getObjectById(objId);
            }

            sectionObj.setVisible(0);
        }
        if (initialTaskWiId != "") {
            var propsText = "<Props>" + "<wiId>" + window.rdSrv.currentWiId + "</wiId>" + "</Props>";

            triggerWS(WebPortal.Plugin.Custom, 'GetWorkflowHistory', '', propsText, function (result) {
                try {
                    

                    for (var i = 0; i < result.childNodes[0].childNodes.length; i++) {
                        var sectionObj = null;
                        if (i == 0) {
                            sectionObj = form.getObjectById("Initial task");
                        } else if (i == 10) {
                            objId = "Task" + i.toString();
                            sectionObj = form.getObjectById(objId);
                        } else {
                            objId = "Task0" + i.toString();
                            sectionObj = form.getObjectById(objId);
                        }

                        sectionObj.setVisible(1);

                        var nodeObj = result.childNodes[0].childNodes[i];

                        if (i == 0) {
                            var initialTaskRoutingList = form.getObjectById("Initial task Routing List");
                            var initialTaskRequestBy = form.getObjectById("Initial task Request By");
                            var initialTaskAssignDate = form.getObjectById("Initial task Assign Date");
                            var initialTaskResponseDate = form.getObjectById("Initial task Response Date");
                            var initialTaskRoutingStatus = form.getObjectById("Initial task Routing Status");
                            var initialTaskToBeDoneBy = form.getObjectById("Initial task To Be Done By");
                            var initialTaskDueDate = form.getObjectById("Initial task Due Date");
                            var initialTaskStatus = form.getObjectById("Initial task Status");

                            initialTaskRoutingList.setData(nodeObj.getAttribute("workflowName"));
                            initialTaskRequestBy.setData(nodeObj.getAttribute("requestBy"));
                            initialTaskAssignDate.setData(nodeObj.getAttribute("assignDate"));
                            initialTaskResponseDate.setData(nodeObj.getAttribute("responseDate"));
                            initialTaskRoutingStatus.setData(nodeObj.getAttribute("workflowStatus"));
                            initialTaskToBeDoneBy.setData(nodeObj.getAttribute("toBeDoneBy"));
                            initialTaskDueDate.setData(nodeObj.getAttribute("dueDate"));
                            initialTaskStatus.setData(nodeObj.getAttribute("taskStatus"));

                        } else {
                            var index = i.toString();
                            if (index.length = 1) {
                                index = "0" + index;
                            }

                            var taskId = "Task" + index;

                            var RequestBy = form.getObjectById(taskId + " Request By");
                            var AssignDate = form.getObjectById(taskId + " Assign Date");
                            var ResponseDate = form.getObjectById(taskId + " Response Date");
                            var ToBeDoneBy = form.getObjectById(taskId + " To Be Done By");
                            var DueDate = form.getObjectById(taskId + " Due Date");
                            var Status = form.getObjectById(taskId + " Status");

                            RequestBy.setData(nodeObj.getAttribute("requestBy"));
                            AssignDate.setData(nodeObj.getAttribute("assignDate"));
                            ResponseDate.setData(nodeObj.getAttribute("responseDate"));
                            ToBeDoneBy.setData(nodeObj.getAttribute("toBeDoneBy"));
                            DueDate.setData(nodeObj.getAttribute("dueDate"));
                            Status.setData(nodeObj.getAttribute("taskStatus"));
                        }
                    }

                } catch (err) { }

            }, FailedCallback);
        }

        return '';
    };

}

// define prototype
DMSEForm.TRM.Eform.GetHistory.prototype = new EFScriptClassBase;
