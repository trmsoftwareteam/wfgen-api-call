﻿Imports DMS.CoreAPI
Imports System.Xml
Imports System.Net
Imports System.Net.Security

Public Class ApexFunction
    Public Shared Function GetApexXml(ByRef caseXmlTag() As String, ByRef workflowXmlTag() As String, ByRef caseDic As Dictionary(Of String, String), ByRef workflowDic As Dictionary(Of String, String)) As XmlDocument
        Dim xml As New XmlDocument()
        Dim request As XmlElement
        request = xml.CreateElement("request")
        xml.AppendChild(request)

        Dim caseXml As XmlElement
        caseXml = xml.CreateElement("Case")
        request.AppendChild(caseXml)
        For Each caseXmlString As String In caseXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(caseXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(caseXmlString, caseDic)
            If xmlEl.InnerText.Contains("&") Then
                xmlEl.InnerText.Replace("&", "&amp;")
            End If
            caseXml.AppendChild(xmlEl)
        Next

        Dim workflowXml As XmlElement
        workflowXml = xml.CreateElement("Workflow")
        request.AppendChild(workflowXml)
        For Each workflowXmlString As String In workflowXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(workflowXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(workflowXmlString, workflowDic)
            If xmlEl.InnerText.Contains("&") Then
                xmlEl.InnerText.Replace("&", "&amp;")
            End If
            workflowXml.AppendChild(xmlEl)
        Next

        Return xml
    End Function

    Public Shared Function GetADName(ByRef wfi As RDWFInstance, ByRef uc As RDUserRoleController, ByRef ComboName As String)
        Dim eformDoc As XmlDocument = GlobalFunction.GetEformXmlDocByWorkflow(wfi)
        If Not eformDoc Is Nothing Then
            Dim parts As String() = GlobalFunction.GetDataByIdAndTag(eformDoc, "Combo", ComboName).Split(New String() {"||"}, StringSplitOptions.RemoveEmptyEntries)
            If parts.Length > 0 Then
                If IsNumeric(parts(0)) Then
                    Return uc.GetUserById(CLng(parts(0))).Name
                End If
            End If
        End If
        Return vbNullString
    End Function

    Public Shared Sub CallDodo(ByRef apiURL As String, ByRef wfi As RDWFInstance, ByRef srv As DMSServer, ByRef xmlDoc As XmlDocument)
        srv.NewException(LogManager.LogLevel.Exception, "Input XML:" + xmlDoc.OuterXml)
        ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(Function() True)
        Dim client As New WebClient
        client.Headers.Add("Content-Type", "application/xml")
        Dim sentXml As Byte() = System.Text.Encoding.ASCII.GetBytes(xmlDoc.OuterXml)
        Dim responseByte As Byte() = client.UploadData(apiURL, "POST", sentXml)
        Dim responseString As String = System.Text.Encoding.ASCII.GetString(responseByte)
        Dim responseXml As XmlDocument = New XmlDocument
        responseXml.LoadXml(responseString)
        srv.NewException(LogManager.LogLevel.Exception, "OutPut Xml:" + responseString)
        If Not responseXml.LastChild.FirstChild.InnerText = "true" Then
            If responseXml.LastChild.ChildNodes.Item(1).Name = "message" Then
                Throw New Exception("API Error:" + responseXml.LastChild.ChildNodes.Item(1).InnerText)
            End If
            Throw New Exception("Error")
        End If

        If Not responseXml.LastChild.ChildNodes.Item(1).InnerText = "Successfully!" Then
            Throw New Exception("Error")
        End If
    End Sub
End Class
