﻿Imports DMS.CoreAPI
Imports GenericDatabase
Public Class EventCust
    Implements IEventHandler
    Dim trmiId As String = "31"
    Dim trmAutoTriggerWorkflowlId As String = "21"

    Public Sub OnObjectCreated(ByRef e As DMS.CoreAPI.EventArgument) Implements DMS.CoreAPI.IEventHandler.OnObjectCreated
        Dim srv As DMSServer = e.Srv
        If e.Target.SecuType = RDSecuObject.RDSecuType.RDDocument Then
            Dim rdDoc As RDDocument = e.Target
            MoveFolder(srv, rdDoc)

        End If
    End Sub

    Public Sub OnObjectCreating(ByRef e As DMS.CoreAPI.EventArgument) Implements DMS.CoreAPI.IEventHandler.OnObjectCreating

    End Sub

    Public Sub OnObjectDeleted(ByRef e As DMS.CoreAPI.EventArgument) Implements DMS.CoreAPI.IEventHandler.OnObjectDeleted

    End Sub

    Public Sub OnObjectDeleting(ByRef e As DMS.CoreAPI.EventArgument) Implements DMS.CoreAPI.IEventHandler.OnObjectDeleting

    End Sub

    Public Sub OnObjectUpdated(ByRef e As DMS.CoreAPI.EventArgument) Implements DMS.CoreAPI.IEventHandler.OnObjectUpdated
        Dim srv As DMSServer = e.Srv
        If e.Target.SecuType = RDSecuObject.RDSecuType.RDDocument Then
            Dim rdDoc As RDDocument = e.Target
            MoveFolder(srv, rdDoc)
            DocNameHandle(srv, rdDoc)

        End If
    End Sub

    Public Sub OnObjectUpdating(ByRef e As DMS.CoreAPI.EventArgument) Implements DMS.CoreAPI.IEventHandler.OnObjectUpdating

    End Sub

    Public Sub OnQueryCompleted(ByRef e As DMS.CoreAPI.EventArgument, ByRef r As DMS.CoreAPI.RDQueryController.QueryResult) Implements DMS.CoreAPI.IEventHandler.OnQueryCompleted

    End Sub

    Public Sub OnQueryStart(ByRef e As DMS.CoreAPI.EventArgument, ByRef qp As DMS.CoreAPI.QueryParameter) Implements DMS.CoreAPI.IEventHandler.OnQueryStart

    End Sub

    Private Sub MoveFolder(ByRef srv As DMSServer, ByRef rdDoc As RDDocument)
        srv.NewException(LogManager.LogLevel.Exception, "MoveFolder01")
        If rdDoc.ParentFolder.Name = "Pending" Then
            srv.NewException(LogManager.LogLevel.Exception, "MoveFolder02")
            If Not String.IsNullOrEmpty(rdDoc.DocProps("Case No")) Then
                srv.NewException(LogManager.LogLevel.Exception, "MoveFolder03")
                Dim BLMNo As String = rdDoc.DocProps("BLM No").Trim
                Dim InjuredName As String = rdDoc.DocProps("Injured Name").Trim
                Dim fc As RDFolderController = srv.CreateController(ControllerType.RDFolderController)
                Dim caseNumberFolder As RDFolder = fc.GetFolderByPath("Home\Case\" + BLMNo + "-" + InjuredName)
                If Not caseNumberFolder Is Nothing Then
                    Dim staffFolder As RDFolder = fc.GetFolderByPath("Home\Case\" + BLMNo + "-" + InjuredName + "\staff", True)
                    rdDoc.MoveTo(staffFolder, RDSecuObject.RDSecuMoveOption.AlignToDestination)
                    AutoTriggerWorkflow(srv, rdDoc)
                End If
            End If
        End If
    End Sub

    Private Sub DocNameHandle(ByRef srv As DMSServer, ByRef rdDoc As RDDocument)
        srv.NewException(LogManager.LogLevel.Exception, "DocNameHandle01")
        If rdDoc.ObjIndexCardId = CLng(trmiId) Then
            srv.NewException(LogManager.LogLevel.Exception, "DocNameHandle02")
            Dim fieldValue As String = ""

            If Not String.IsNullOrEmpty(rdDoc.DocProps("High Level No")) Or Not String.IsNullOrEmpty(rdDoc.DocProps("Basic Level No")) Then
                fieldValue += "-" + rdDoc.DocProps("High Level No") + "/" + rdDoc.DocProps("Basic Level No")
            End If

            If Not String.IsNullOrEmpty(rdDoc.DocProps("Injured Name")) Then
                fieldValue += "-" + rdDoc.DocProps("Injured Name")
            End If

            If Not String.IsNullOrEmpty(rdDoc.CreateDate.ToString("yyyyMMdd")) Then
                fieldValue += "-" + rdDoc.CreateDate.ToString("yyyyMMdd")
            End If

            If Not String.IsNullOrEmpty(rdDoc.DocProps("Document Type")) Then
                fieldValue += "-" + rdDoc.DocProps("Document Type")
            End If
            srv.NewException(LogManager.LogLevel.Exception, "Checkpoint03")
            Dim fieldList As New List(Of RDIndexField)

            For index As Integer = 0 To rdDoc.ObjIndexCard.IndexFields.Count - 1
                For Each rdIndexField As RDIndexField In rdDoc.ObjIndexCard.IndexFields
                    If index = rdIndexField.FieldPosition Then
                        If rdIndexField.Name <> "High Level No" _
                            And rdIndexField.Name <> "Basic Level No" _
                            And rdIndexField.Name <> "Injured Name" _
                            And rdIndexField.Name <> "Document Type" _
                            And rdIndexField.Name <> "Doc Name" Then

                            If Not String.IsNullOrEmpty(rdDoc.DocProps(rdIndexField.Name)) Then
                                fieldValue += "-" + rdDoc.DocProps(rdIndexField.Name)
                            End If
                        End If
                    End If
                Next
            Next

            srv.NewException(LogManager.LogLevel.Exception, "Checkpoint03")
            If fieldValue.Substring(0, 1) = "-" Then
                fieldValue = fieldValue.Substring(1)
            End If
            srv.NewException(LogManager.LogLevel.Exception, "Checkpoint04")
            srv.NewException(LogManager.LogLevel.Exception, "doc:" + rdDoc.DocProps("Doc Name"))
            srv.NewException(LogManager.LogLevel.Exception, "fieldValue:" + fieldValue)
            If rdDoc.DocProps("Doc Name") <> fieldValue Then
                srv.NewException(LogManager.LogLevel.Exception, "Checkpoint05")
                rdDoc.DocProps("Doc Name") = fieldValue
                rdDoc.Update()
            End If
        End If
    End Sub

    Private Sub AutoTriggerWorkflow(ByRef srv As DMSServer, ByRef rdDoc As RDDocument)
        If Not String.IsNullOrEmpty(rdDoc.DocProps("Document Type")) Then
            Dim recordList As List(Of Dictionary(Of String, String)) = GlobalFunction.GetIndexLookUpRecord(srv, trmAutoTriggerWorkflowlId)
            If recordList.Count > 0 Then
                Dim workflowName As String = ""
                For Each dic As Dictionary(Of String, String) In recordList
                    If dic.Item("Document Type") = rdDoc.DocProps("Document Type") Then
                        workflowName = dic.Item("Workflow Name")
                        Exit For
                    End If
                Next

                If Not String.IsNullOrEmpty(workflowName) Then
                    Dim workflowDoc As RDDocument = GlobalFunction.GetWorkflowObj(srv, workflowName)
                    If Not workflowDoc Is Nothing Then
                        GlobalFunction.StartNewInstanceInternal(srv, workflowDoc, rdDoc)
                    End If
                End If
            End If


        End If
    End Sub

End Class
