﻿Imports System.IO
Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml
Imports System.Net
Imports System.Net.Security
Public Class GlobalFunction
    Public Shared Function GetIndexLookUpRecord(ByRef srv As DMS.CoreAPI.DMSServer, ByRef lId As Long) As List(Of Dictionary(Of String, String))
        Dim recordList As New List(Of Dictionary(Of String, String))
        Dim icc As RDIndexCardController = srv.CreateController(ControllerType.RDIndexCardController)
        Dim l As RDLookup = icc.GetLookupById(lId)
        Dim pg As RDPropertyGroup = l.GetPropertyGroup()

        For Each rdPropertyItem As RDPropertyItem In pg.GetLists(0, 0)
            Dim recordDic As New Dictionary(Of String, String)
            For Each objIndexField As RDIndexField In rdPropertyItem.ObjIndexCard.IndexFields
                recordDic.Add(objIndexField.ID.ToString, rdPropertyItem.DocumentProperty(objIndexField.ID))
            Next
            recordList.Add(recordDic)
        Next

        Return recordList
    End Function

    Public Shared Function GetWorkflowObj(ByRef srv As DMSServer, ByRef workflowName As String) As RDDocument
        Dim rootList As RDCollection = srv.LoginSession.LoginUser.GetOrganizationGroup.GetSysFolders
        Dim fc As RDFolderController = srv.CreateController(ControllerType.RDFolderController)
        If rootList Is Nothing Then
            Return Nothing
        End If

        For Each f As RDFolder In rootList
            Dim c As RDFolder = fc.GetFolderByPath(f, "Workflow templates")
            If Not c Is Nothing Then
                Dim qc As RDQueryController = srv.CreateController(ControllerType.RDQueryController)
                Dim ex As New RDExpression(srv)
                ex.AddExp(New RDCriteria(srv, "D.documentExtension", FieldType.Varchar, "=", ".rwt"), "and")
                ex.AddExp(New RDCriteria(srv, "S.objName", FieldType.Varchar, "=", workflowName), "and")
                Dim result As RDCollection = qc.QueryObjects(c, 0, 9999, c.SortField, c.SortAsc, c.DefaultIndexCard, c.ShowPageCount, ex, True, False).Result
                If Not result Is Nothing Then
                    If result.Count > 0 Then
                        Return result.Item(0)
                    End If
                End If

            End If
        Next
        Return Nothing
    End Function

    Public Shared Function StartNewInstanceInternal(ByRef srv As DMSServer, ByRef template As RDDocument, ByRef attachment As RDDocument) As RDWFInstance


        Dim dc As RDDocumentController = srv.CreateController(ControllerType.RDDocumentController)



        Dim doclist As New RDCollection(srv, RDCollection.RDType.RDDocument)
        doclist.Add(attachment)


        ' start the workflow
        Dim wfic As RDWFInstanceController = srv.CreateController(ControllerType.RDWFInstanceController)
        Dim wfi As RDWFInstance = wfic.CreateNewWFInstance(template, doclist, Nothing, Nothing, 50)

        Return wfi

    End Function

    Public Shared Function GetEformDoc(ByRef wfi As RDWFInstance) As RDDocument
        Try
            For Each rdSecu As RDSecuObject In wfi.GetRelations
                If rdSecu.SecuType = RDSecuObject.RDSecuType.RDDocument Then
                    Dim rdDoc As RDDocument = rdSecu
                    Dim v As RDVersion = rdDoc.GetVersion
                    Dim ext As String = v.Extension.ToLower
                    If ext = ".rfti" Then
                        Return rdDoc
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
        Return Nothing
    End Function

    Public Shared Function CreateNewPdfDoc(ByRef srv As DMSServer, ByRef docName As String, ByRef parentFolder As RDFolder, ByRef eformDoc As RDDocument) As RDDocument
        Dim dc As RDDocumentController = srv.CreateController(ControllerType.RDDocumentController)
        Dim newEformDoc As RDDocument = dc.CreateDocument(docName, parentFolder)

        Dim rdVer As RDVersion = eformDoc.GetVersion
        Dim doc As New System.Xml.XmlDocument
        doc.LoadXml(rdVer.GetContentXml().OuterXml)

        For Each childNode As System.Xml.XmlNode In doc.FirstChild.ChildNodes
            If childNode.Name = "Layout" Then
                For Each childNode2 As System.Xml.XmlNode In childNode
                    If childNode2.Name = "Page" Then
                        If childNode2.Attributes("title").Value = "Approval History" Then
                            childNode.RemoveChild(childNode2)
                        End If
                    End If

                Next
            End If

        Next
        Dim f As New DMS.EForm.EForm(doc.FirstChild, "")
        Dim ms As New IO.MemoryStream
        Dim filePath As String = "C:\Temp\" + Guid.NewGuid.ToString()
        f.SaveAsPDF(filePath)

        newEformDoc.AddFileContent(New DMSStream(filePath, True), eformDoc.Name.Replace(".rfti", ".pdf"), "", ".pdf")
        newEformDoc.Update()
        Return newEformDoc
    End Function

    Public Shared Sub CopyIndexField(ByRef souDoc As RDDocument, ByRef destDoc As RDDocument)
        For Each destIndexField As RDIndexField In destDoc.ObjIndexCard.IndexFields
            For Each souIndexField As RDIndexField In souDoc.ObjIndexCard.IndexFields
                If destIndexField.Name = souIndexField.Name Then
                    If Not String.IsNullOrEmpty(souDoc.DocProps(souIndexField.Name)) Then
                        destDoc.DocProps(destIndexField.Name) = souDoc.DocProps(souIndexField.Name)
                    End If
                End If
            Next
        Next
        destDoc.Update()
    End Sub

    Public Shared Function GetFinalResponseResult(ByRef wfi As RDWFInstance) As Dictionary(Of String, String)
        Dim dic As New Dictionary(Of String, String)
        dic.Add("result", "")
        dic.Add("taskName", "")
        dic.Add("perviouRecipientName", "")
        dic.Add("recipientName", "")

        Dim lastResultDate As Date
        For Each wfit As RDWFInstanceTask In wfi.TasksList
            For Each wfitr As RDWFInstanceTaskRecipient In wfit.Recipients
                If wfitr.Status = RDWFInstanceTaskRecipient.RecipientStatus.Response Then
                    If String.IsNullOrEmpty(dic.Item("result")) Then
                        dic.Item("result") = wfitr.Result
                        dic.Item("taskName") = wfit.Label
                        dic.Item("perviouRecipientName") = dic.Item("recipientName")
                        dic.Item("recipientName") = wfitr.User.Name
                        lastResultDate = wfitr.ResponseDate


                    Else
                        If lastResultDate < wfitr.ResponseDate Then
                            dic.Item("result") = wfitr.Result
                            dic.Item("taskName") = wfit.Label
                            dic.Item("perviouRecipientName") = dic.Item("recipientName")
                            dic.Item("recipientName") = wfitr.User.Name
                            lastResultDate = wfitr.ResponseDate
                        End If
                    End If

                End If
            Next
        Next
        Return dic
    End Function

    Public Shared Function GetSalfString(ByRef key As String, ByRef dic As Dictionary(Of String, String)) As String
        Try
            Return dic.Item(key)
        Catch ex As Exception

        End Try
        Return ""
    End Function

    Public Shared Function GetEformXmlDocByWorkflow(ByRef wfi As RDWFInstance) As XmlDocument
        Dim xmlDoc As XmlDocument = Nothing
        Try
            For Each rdDoc As RDDocument In wfi.GetRelations
                Dim v As RDVersion = rdDoc.GetVersion
                Dim ext As String = v.Extension.ToLower
                If ext = ".rfti" Then
                    Dim eform As DMS.EForm.EForm = v.GetEFormContent
                    Return eform.GetXmlDocument()
                End If
            Next



        Catch ex As Exception

        End Try

        If xmlDoc Is Nothing Then
            Throw New Exception("GetEformXmlDocByWorkflow Error")
        Else
            Return xmlDoc
        End If
    End Function

    Public Shared Function GetDataByIdAndTag(ByRef xmlDoc As Xml.XmlDocument, ByRef tag As String, ByRef id As String) As String
        Dim isSuccess As Boolean = False

        Try
            Dim xmlNodeList As Xml.XmlNodeList = xmlDoc.GetElementsByTagName(tag)

            For Each xmlElement As Xml.XmlElement In xmlNodeList
                If xmlElement.Attributes("id").Value = id Then
                    Return xmlElement.Attributes("value").Value
                    ' Return xmlElement.FirstChild.InnerText

                End If
            Next
        Catch ex As Exception

        End Try
        Return ""

    End Function

    Public Shared Function GetLastComment(ByRef xmlDoc As Xml.XmlDocument) As String
        Dim lastComment As String = ""
        For index As Integer = 0 To 10
            Dim parentID As String = ""
            If index = 0 Then
                parentID = "Initial task "
            ElseIf index = 10 Then
                parentID = "Task10 "
            Else
                parentID = "Task0" + index.ToString + " "
            End If

            Dim requestBy As String = GetDataByIdAndTag(xmlDoc, "Label", parentID + "Request By")
            If Not String.IsNullOrEmpty(requestBy) Then
                lastComment = ""
                lastComment = GetDataByIdAndTag(xmlDoc, "Textbox", parentID + "Comment")
            End If
        Next

        Return lastComment
    End Function

End Class
