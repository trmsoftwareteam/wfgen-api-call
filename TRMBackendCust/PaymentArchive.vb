﻿Imports DMS.CoreAPI
Public Class PaymentArchive
    Implements DMS.CoreAPI.IWFScriptTask

    Public ReadOnly Property TaskId As String Implements DMS.CoreAPI.IWFScriptTask.TaskId
        Get
            Return "PaymentArchive"
        End Get
    End Property

    Public Sub Process(wfi As DMS.CoreAPI.RDWFInstance, srv As DMS.CoreAPI.DMSServer) Implements DMS.CoreAPI.IWFScriptTask.Process
        Dim fc As RDFolderController = srv.CreateController(ControllerType.RDFolderController)
        Dim ic As RDIndexCardController = srv.CreateController(ControllerType.RDIndexCardController)
        Dim dc As RDDocumentController = srv.CreateController(ControllerType.RDDocumentController)
        Dim workflow As RDFolder = fc.GetFolderByPath("Home\Case\" + wfi.DocProps("BLM No") + "-" + wfi.DocProps("Injured Name") + "\Workflow", False)

        If Not workflow Is Nothing Then
            Dim rdIndexCard As RDIndexCard = ic.GetIndexCardByName("TRM Workflow Payment Application Archive")
            Dim paymentFolder As RDFolder = fc.GetFolderByPath("Home\Case\" + wfi.DocProps("BLM No") + "-" + wfi.DocProps("Injured Name") + "\Workflow\Payment", False)
            If paymentFolder Is Nothing Then
                paymentFolder = fc.CreateFolder("Payment", workflow, rdIndexCard)
                paymentFolder.IsPropagateIndex = True
                paymentFolder.Update()
            End If


            Dim eformDoc As RDDocument = GlobalFunction.GetEformDoc(wfi)
            Dim newEformDoc As RDDocument = GlobalFunction.CreateNewPdfDoc(srv, eformDoc.Name, paymentFolder, eformDoc)
            GlobalFunction.CopyIndexField(eformDoc, newEformDoc)


        Else
            Dim errorFolder As RDFolder = fc.GetFolderByPath("Home\Error\Workflow\Payment", False)
            Dim eformDoc As RDDocument = GlobalFunction.GetEformDoc(wfi)
            Dim newEformDoc As RDDocument = GlobalFunction.CreateNewPdfDoc(srv, eformDoc.Name, errorFolder, eformDoc)
            GlobalFunction.CopyIndexField(eformDoc, newEformDoc)
        End If

    End Sub

End Class
