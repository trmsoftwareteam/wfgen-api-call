﻿Imports DMS.CoreAPI
Imports System.Xml
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.IO
Public Class PaymentDoDoAPI
    Implements DMS.CoreAPI.IWFScriptTask
    Dim apiURL As String = "https://trmtest.embraiz.com/TRM/dodo/createPayment"
    Dim caseXmlTag() As String = {"caseId", "finalPayment", "paymentMethod", "targetPayDate", "referenceNo", "description", "remark", "paymentType", "deliveyOption", "deliveryId", "clientNum", "deliveryParty", "deliveryAdd1", "deliveryAdd2", "deliveryAdd3", "payeeId", "chequeDetail1", "chequeDetail2", "chequeDetail3", "chequeDetail4", "chequeDetail5", "chequeDetail6", "advancePayment", "settlementDate"}
    Dim reserveXmlTag() As String = {"AF", "AT", "CO", "CP", "DE", "LE", "L1", "ME", "ML", "M2", "PF", "PI"}
    Dim workflowXmlTag() As String = {"eventId", "creator", "approvedBy", "assignedToUser", "action", "comment"}
    Public ReadOnly Property TaskId As String Implements DMS.CoreAPI.IWFScriptTask.TaskId
        Get
            Return "PaymentDoDoAPI"
        End Get
    End Property

    Private Function GetCaseMapDic() As Dictionary(Of String, String)
        Dim dic As New Dictionary(Of String, String)
        dic.Add("Case ID", "caseId")
        dic.Add("Final Payment", "finalPayment")
        dic.Add("Payment Method", "paymentMethod")
        dic.Add("Target Pay Date", "targetPayDate")
        dic.Add("Reference No", "referenceNo")
        dic.Add("Description", "description")
        dic.Add("Remark", "remark")
        dic.Add("Payment Type", "paymentType")
        dic.Add("Delivery Option ID", "deliveyOption")
        dic.Add("Delivery ID", "deliveryId")
        dic.Add("Client Number Delivery", "clientNum")
        dic.Add("Delivery Party", "deliveryParty")
        dic.Add("Delivery Address 1", "deliveryAdd1")
        dic.Add("Delivery Address 2", "deliveryAdd2")
        dic.Add("Delivery Address 3", "deliveryAdd3")
        dic.Add("Payee ID", "payeeId")
        dic.Add("Cheque Detail 1", "chequeDetail1")
        dic.Add("Cheque Detail 2", "chequeDetail2")
        dic.Add("Cheque Detail 3", "chequeDetail3")
        dic.Add("Cheque Detail 4", "chequeDetail4")
        dic.Add("Cheque Detail 5", "chequeDetail5")
        dic.Add("Cheque Detail 6", "chequeDetail6")
        dic.Add("Advance Payment", "advancePayment")
        dic.Add("Settlement or Invoice Date", "settlementDate")
        Return dic
    End Function

    Private Function GelXml(ByRef caseDic As Dictionary(Of String, String), ByRef reserveDic As Dictionary(Of String, String), ByRef workflowDic As Dictionary(Of String, String)) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim request As XmlElement
        request = xml.CreateElement("request")
        xml.AppendChild(request)

        Dim caseXml As XmlElement
        caseXml = xml.CreateElement("case")
        request.AppendChild(caseXml)

        For Each caseXmlString As String In caseXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(caseXmlString)
            If caseXmlString = "finalPayment" Then
                If GlobalFunction.GetSalfString(caseXmlString, caseDic) = "Yes" Then
                    xmlEl.InnerText = "1"
                Else
                    xmlEl.InnerText = "0"
                End If

            Else
                xmlEl.InnerText = GlobalFunction.GetSalfString(caseXmlString, caseDic)
            End If

            caseXml.AppendChild(xmlEl)
        Next

        If Not reserveDic Is Nothing Then
            Dim reserveXml As XmlElement
            reserveXml = xml.CreateElement("payment")
            request.AppendChild(reserveXml)

            For Each reserveXmlString As String In reserveXmlTag
                Dim xmlEl As XmlElement
                xmlEl = xml.CreateElement(reserveXmlString)
                xmlEl.InnerText = GlobalFunction.GetSalfString(reserveXmlString, reserveDic)
                reserveXml.AppendChild(xmlEl)
            Next
        End If



        Dim workflowXml As XmlElement
        workflowXml = xml.CreateElement("workflow")
        request.AppendChild(workflowXml)

        For Each workflowXmlString As String In workflowXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(workflowXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(workflowXmlString, workflowDic)
            workflowXml.AppendChild(xmlEl)
        Next

        Return xml
    End Function

    Private Function GetSubmitXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String, ByRef firstApprover As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        For Each key As String In GetCaseMapDic().Keys
            caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
        Next

        Dim reserveDic As New Dictionary(Of String, String)
        For Each newReservekey As String In reserveXmlTag
            If wfi.DocProps(newReservekey) <> "0" Then
                reserveDic.Add(newReservekey, wfi.DocProps(newReservekey))
            End If
        Next

        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", wfi.Owner.Name)
        workflowDic.Add("assignedToUser", firstApprover)
        If Not String.IsNullOrEmpty(wfi.DocProps("Event ID")) Then
            workflowDic.Add("approvedBy", wfi.Owner.Name)

        End If

        workflowDic.Add("action", "submit")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, reserveDic, workflowDic)
    End Function

    Private Function GetApproveXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String, ByRef srv As DMSServer) As XmlDocument
        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        Dim assignedToUser As String = ""

        Dim role As RDRole = uc.GetRoleByName("Finance Approval 9")
        If Not role Is Nothing Then
            Dim user As RDUser = role.GetAllUsers(False).Item(0)
            If Not user Is Nothing Then
                assignedToUser = user.Name
            End If
        End If


        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        caseDic.Add("caseId", wfi.DocProps("Case ID"))


        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", assignedToUser)
        workflowDic.Add("action", "approve")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, Nothing, workflowDic)
    End Function

    Private Function GetRejectXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        caseDic.Add("caseId", wfi.DocProps("Case ID"))


        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", creator)
        workflowDic.Add("action", "reject")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, Nothing, workflowDic)
    End Function

    Private Function GetFinalXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        caseDic.Add("caseId", wfi.DocProps("Case ID"))


        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", "")
        workflowDic.Add("action", "final")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, Nothing, workflowDic)
    End Function

    Private Function GetAbortXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        For Each key As String In GetCaseMapDic().Keys
            caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
        Next

        Dim reserveDic As New Dictionary(Of String, String)
        For Each newReservekey As String In reserveXmlTag
            If wfi.DocProps(newReservekey) <> "0" Then
                reserveDic.Add(newReservekey, wfi.DocProps(newReservekey))
            End If
        Next

        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", "")
        workflowDic.Add("action", "abort")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, reserveDic, workflowDic)
    End Function

    Public Sub CallDodo(ByRef wfi As RDWFInstance, ByRef srv As DMSServer, ByRef xmlDoc As XmlDocument)
        srv.NewException(LogManager.LogLevel.Exception, "Input XML:" + xmlDoc.OuterXml)
        ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(Function() True)
        Dim client As New WebClient
        client.Headers.Add("Content-Type", "application/xml")
        Dim sentXml As Byte() = System.Text.Encoding.ASCII.GetBytes(xmlDoc.OuterXml)
        Dim responseByte As Byte() = client.UploadData(apiURL, "POST", sentXml)
        Dim responseString As String = System.Text.Encoding.ASCII.GetString(responseByte)
        Dim responseXml As XmlDocument = New XmlDocument
        responseXml.LoadXml(responseString)
        srv.NewException(LogManager.LogLevel.Exception, "OutPut Xml:" + responseString)
        If Not responseXml.LastChild.FirstChild.InnerText = "true" Then
            If responseXml.LastChild.ChildNodes.Item(1).Name = "message" Then
                Throw New Exception("API Error:" + responseXml.LastChild.ChildNodes.Item(1).InnerText)
            End If
            Throw New Exception("Error")
        End If

        If Not responseXml.LastChild.ChildNodes.Item(1).InnerText = "Successfully!" Then
            Throw New Exception("Error")
        End If

        If String.IsNullOrEmpty(wfi.DocProps("Event ID")) Then

            wfi.DocProps("Event ID") = responseXml.LastChild.LastChild.InnerText
        End If
    End Sub


    Public Sub Process(wfi As DMS.CoreAPI.RDWFInstance, srv As DMS.CoreAPI.DMSServer) Implements DMS.CoreAPI.IWFScriptTask.Process

        Try
            If Not String.IsNullOrEmpty(srv.GetCustomSetting("Payment API URL")) Then
                apiURL = srv.GetCustomSetting("Payment API URL")
            End If
            Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
            wfi.DocProps("Error") = ""
            Dim lastTaskInfo As Dictionary(Of String, String) = GlobalFunction.GetFinalResponseResult(wfi)
            If String.IsNullOrEmpty(lastTaskInfo.Item("result")) Then
                Throw New Exception("Last Result Not Found")
            End If


            Dim lastComment As String = ""
            Dim firstApprover As String = ""
            Dim eformDoc As XmlDocument = GlobalFunction.GetEformXmlDocByWorkflow(wfi)
            If Not eformDoc Is Nothing Then
                lastComment = GlobalFunction.GetLastComment(eformDoc)
                Dim parts As String() = GlobalFunction.GetDataByIdAndTag(eformDoc, "Combo", "First Approver").Split(New String() {"||"}, StringSplitOptions.RemoveEmptyEntries)
                If parts.Length > 0 Then
                    If IsNumeric(parts(0)) Then
                        Dim user As RDUser = uc.GetUserById(CLng(parts(0)))
                        firstApprover = user.Name
                    End If
                End If

            End If

            Dim xmlDoc As XmlDocument = Nothing
            If lastTaskInfo.Item("result") = "Submit" Then
                xmlDoc = GetSubmitXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment, firstApprover)
                CallDodo(wfi, srv, xmlDoc)
            ElseIf lastTaskInfo.Item("result") = "Abort" And Not String.IsNullOrEmpty(wfi.DocProps("Event ID")) Then
                xmlDoc = GetAbortXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment)
                CallDodo(wfi, srv, xmlDoc)
            ElseIf lastTaskInfo.Item("result") = "Reject" Then
                xmlDoc = GetRejectXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment)
                CallDodo(wfi, srv, xmlDoc)
            ElseIf lastTaskInfo.Item("result") = "Approve" Then
                If lastTaskInfo.Item("taskName") = "External Head of Claims" Or lastTaskInfo.Item("taskName") = "First Approver" And CLng(wfi.DocProps("Max Sum")) <= 7800000 Then
                    'xmlDoc = GetApproveXml(wfi, lastTaskInfo.Item("recipientName"))
                    'CallDodo(wfi, srv, xmlDoc)
                    xmlDoc = GetFinalXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment)
                    CallDodo(wfi, srv, xmlDoc)
                Else
                    xmlDoc = GetApproveXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment, srv)
                    CallDodo(wfi, srv, xmlDoc)
                End If
            End If

        Catch ex As Exception
            'wfi.StatusMsg = ex.Message
            wfi.DocProps("Error") = ex.Message
        End Try




    End Sub


End Class
