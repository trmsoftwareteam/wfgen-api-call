﻿Imports DMS.CoreAPI

Public Class ReserveArchive
    Implements DMS.CoreAPI.IWFScriptTask

    Public ReadOnly Property TaskId As String Implements DMS.CoreAPI.IWFScriptTask.TaskId
        Get
            Return "ReserveArchive"
        End Get
    End Property

    Public Sub Process(wfi As DMS.CoreAPI.RDWFInstance, srv As DMS.CoreAPI.DMSServer) Implements DMS.CoreAPI.IWFScriptTask.Process
        Dim fc As RDFolderController = srv.CreateController(ControllerType.RDFolderController)
        Dim ic As RDIndexCardController = srv.CreateController(ControllerType.RDIndexCardController)
        Dim dc As RDDocumentController = srv.CreateController(ControllerType.RDDocumentController)


        Dim workflow As RDFolder = fc.GetFolderByPath("Home\Case\" + wfi.DocProps("BLM No") + "-" + wfi.DocProps("Injured Name") + "\Workflow", False)
        If Not workflow Is Nothing Then
            Dim rdIndexCard As RDIndexCard = ic.GetIndexCardByName("TRM Workflow Reserve Application Archive")

            Dim reserveFolder As RDFolder = fc.GetFolderByPath("Home\Case\" + wfi.DocProps("BLM No") + "-" + wfi.DocProps("Injured Name") + "\Workflow\Reserve", False)

            If reserveFolder Is Nothing Then
                reserveFolder = fc.CreateFolder("Reserve", workflow, rdIndexCard)
                reserveFolder.IsPropagateIndex = True
                reserveFolder.Update()
            End If

            For Each rdSecu As RDSecuObject In wfi.GetRelations
                If rdSecu.SecuType = RDSecuObject.RDSecuType.RDDocument Then
                    Dim rdDoc As RDDocument = rdSecu
                    Dim v As RDVersion = rdDoc.GetVersion
                    Dim ext As String = v.Extension.ToLower
                    If ext = ".rfti" Then
                        Dim eformDoc As RDDocument = rdDoc
                        Dim newEformDoc As RDDocument = GlobalFunction.CreateNewPdfDoc(srv, eformDoc.Name, reserveFolder, eformDoc)


                        GlobalFunction.CopyIndexField(eformDoc, newEformDoc)

                    End If
                End If
            Next
        Else
            Dim errorFolder As RDFolder = fc.GetFolderByPath("Home\Error\Workflow\Reserve", False)
            For Each rdSecu As RDSecuObject In wfi.GetRelations
                If rdSecu.SecuType = RDSecuObject.RDSecuType.RDDocument Then
                    Dim rdDoc As RDDocument = rdSecu
                    Dim v As RDVersion = rdDoc.GetVersion
                    Dim ext As String = v.Extension.ToLower
                    If ext = ".rfti" Then
                        Dim eformDoc As RDDocument = rdDoc
                        Dim newEformDoc As RDDocument = GlobalFunction.CreateNewPdfDoc(srv, eformDoc.Name, errorFolder, eformDoc)


                        GlobalFunction.CopyIndexField(eformDoc, newEformDoc)

                    End If
                End If
            Next
        End If





    End Sub

End Class
