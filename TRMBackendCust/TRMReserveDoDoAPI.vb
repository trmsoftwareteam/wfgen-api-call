﻿Imports DMS.CoreAPI
Imports System.Xml
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.IO

Public Class TRMReserveDoDoAPI
    Implements DMS.CoreAPI.IWFScriptTask
    Dim caseXmlTag() As String = {"caseId", "description", "salary", "estsl", "estpd", "code", "reason", "isDeath", "ApexAction"}
    Dim reserveXmlTag() As String = {"AF", "AT", "CO", "CP", "DE", "LE", "L1", "ME", "ML", "M2", "PF", "PI"}
    Dim workflowXmlTag() As String = {"eventId", "creator", "approvedBy", "assignedToUser", "action", "comment"}
    Dim reopenXmlTag() As String = {"ReopenCode", "ClaimsLifeCycle", "ApexSubType", "PresentPosition", "ChangeReason"}
    Dim apiURL As String = "https://trmtest.embraiz.com/TRM/dodo/createReserve"

    Public ReadOnly Property TaskId As String Implements DMS.CoreAPI.IWFScriptTask.TaskId
        Get
            Return "TRMReserveDoDoAPI"
        End Get
    End Property

    Private Function GetCaseMapDic() As Dictionary(Of String, String)
        Dim dic As New Dictionary(Of String, String)
        dic.Add("Case ID", "caseId")
        dic.Add("Description", "description")
        dic.Add("Salary", "salary")
        dic.Add("Est. Sick Leave Days", "estsl")
        dic.Add("PD", "estpd")
        dic.Add("Modification Code", "code")
        dic.Add("Modification Reason", "reason")
        dic.Add("Death", "isDeath")
        Return dic
    End Function

    Private Function GelXml(ByRef caseDic As Dictionary(Of String, String), ByRef reserveDic As Dictionary(Of String, String), ByRef workflowDic As Dictionary(Of String, String)) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim request As XmlElement
        request = xml.CreateElement("request")
        xml.AppendChild(request)

        Dim caseXml As XmlElement
        caseXml = xml.CreateElement("case")
        request.AppendChild(caseXml)

        For Each caseXmlString As String In caseXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(caseXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(caseXmlString, caseDic)
            caseXml.AppendChild(xmlEl)
        Next

        If Not reserveDic Is Nothing Then
            Dim reserveXml As XmlElement
            reserveXml = xml.CreateElement("reserve")
            request.AppendChild(reserveXml)

            For Each reserveXmlString As String In reserveXmlTag
                Dim xmlEl As XmlElement
                xmlEl = xml.CreateElement(reserveXmlString)
                xmlEl.InnerText = GlobalFunction.GetSalfString(reserveXmlString, reserveDic)
                reserveXml.AppendChild(xmlEl)
            Next
        End If

        Dim workflowXml As XmlElement
        workflowXml = xml.CreateElement("workflow")
        request.AppendChild(workflowXml)

        For Each workflowXmlString As String In workflowXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(workflowXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(workflowXmlString, workflowDic)
            workflowXml.AppendChild(xmlEl)
        Next

        Return xml
    End Function

    Private Function GetSubmitXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String, ByRef firstApprover As String, ByRef srv As DMSServer) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        For Each key As String In GetCaseMapDic().Keys
            srv.NewException(3, "key:" + key)
            If key = "Modification Reason" Then
                srv.NewException(3, "Modification Code:" + wfi.DocProps("Modification Code"))
                If wfi.DocProps("Modification Code") = "IR" Then
                    srv.NewException(3, "Modification Code1:" + wfi.DocProps(key))
                    caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
                Else
                    srv.NewException(3, "Modification Code2:" + wfi.DocProps("Other Modification Reason"))
                    caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps("Other Modification Reason"))
                End If

            Else
                caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
            End If

        Next

        Dim reserveDic As New Dictionary(Of String, String)
        For Each newReservekey As String In reserveXmlTag
            If wfi.DocProps(newReservekey) <> wfi.DocProps(newReservekey + " (Original)") Then
                reserveDic.Add(newReservekey, wfi.DocProps(newReservekey + " (Adjust Amount)"))
            End If
        Next

        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", wfi.Owner.Name)
        workflowDic.Add("assignedToUser", firstApprover)
        If Not String.IsNullOrEmpty(wfi.DocProps("Event ID")) Then
            workflowDic.Add("approvedBy", wfi.Owner.Name)

        End If

        workflowDic.Add("action", "submit")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, reserveDic, workflowDic)
    End Function

    Private Function GetApproveXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String, ByRef srv As DMSServer) As XmlDocument
        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        Dim assignedToUser As String = ""

        Dim role As RDRole = uc.GetRoleByName("Finance Approval 9")
        If Not role Is Nothing Then
            Dim user As RDUser = role.GetAllUsers(False).Item(0)
            If Not user Is Nothing Then
                assignedToUser = user.Name
            End If
        End If

        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        caseDic.Add("caseId", wfi.DocProps("Case ID"))


        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", assignedToUser)
        workflowDic.Add("action", "approve")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, Nothing, workflowDic)
    End Function

    Private Function GetRejectXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        caseDic.Add("caseId", wfi.DocProps("Case ID"))


        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", creator)
        workflowDic.Add("action", "reject")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, Nothing, workflowDic)
    End Function

    Private Function GetFinalXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        caseDic.Add("caseId", wfi.DocProps("Case ID"))


        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", "")
        workflowDic.Add("action", "final")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, Nothing, workflowDic)
    End Function

    Private Function GelReopenXml(ByRef caseDic As Dictionary(Of String, String), ByRef workflowDic As Dictionary(Of String, String), ByRef reopenDic As Dictionary(Of String, String)) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim request As XmlElement
        request = xml.CreateElement("request")
        xml.AppendChild(request)

        Dim caseXml As XmlElement
        caseXml = xml.CreateElement("case")
        request.AppendChild(caseXml)

        For Each caseXmlString As String In caseXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(caseXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(caseXmlString, caseDic)
            caseXml.AppendChild(xmlEl)
        Next

        Dim workflowXml As XmlElement
        workflowXml = xml.CreateElement("workflow")
        request.AppendChild(workflowXml)

        For Each workflowXmlString As String In workflowXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(workflowXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(workflowXmlString, workflowDic)
            workflowXml.AppendChild(xmlEl)
        Next

        Dim reopenXml As XmlElement
        reopenXml = xml.CreateElement("Reopen")
        request.AppendChild(reopenXml)

        For Each reopenXmlString As String In reopenXmlTag
            Dim xmlEl As XmlElement
            xmlEl = xml.CreateElement(reopenXmlString)
            xmlEl.InnerText = GlobalFunction.GetSalfString(reopenXmlString, reopenDic)
            reopenXml.AppendChild(xmlEl)
        Next

        Return xml
    End Function

    Private Function GetSubmitReopenXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String, ByRef firstApprover As String, ByRef srv As DMSServer) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        For Each key As String In GetCaseMapDic().Keys
            srv.NewException(3, "key:" + key)
            If key = "Modification Reason" Then
                srv.NewException(3, "Modification Code:" + wfi.DocProps("Modification Code"))
                If wfi.DocProps("Modification Code") = "IR" Then
                    srv.NewException(3, "Modification Code1:" + wfi.DocProps(key))
                    caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
                Else
                    srv.NewException(3, "Modification Code2:" + wfi.DocProps("Other Modification Reason"))
                    caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps("Other Modification Reason"))
                End If

            Else
                caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
            End If

        Next
        caseDic.Add("ApexAction", "toReopen")

        Dim reserveDic As New Dictionary(Of String, String)
        For Each newReservekey As String In reserveXmlTag
            If wfi.DocProps(newReservekey) <> wfi.DocProps(newReservekey + " (Original)") Then
                reserveDic.Add(newReservekey, wfi.DocProps(newReservekey + " (Adjust Amount)"))
            End If
        Next

        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", wfi.Owner.Name)
        workflowDic.Add("assignedToUser", firstApprover)
        If Not String.IsNullOrEmpty(wfi.DocProps("Event ID")) Then
            workflowDic.Add("approvedBy", wfi.Owner.Name)

        End If

        workflowDic.Add("action", "submit")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, reserveDic, workflowDic)
    End Function
    Private Function GetReopenXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()

        Dim caseDic As New Dictionary(Of String, String)
        caseDic.Add("caseId", wfi.DocProps("Case ID"))
        caseDic.Add("ApexAction", "toReopen")

        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", "")
        workflowDic.Add("action", "final")
        workflowDic.Add("comment", comment)

        Dim reopenDic As New Dictionary(Of String, String)
        reopenDic.Add("ReopenCode", wfi.DocProps("Reopen Code"))
        reopenDic.Add("ClaimsLifeCycle", wfi.DocProps("Reopen Claims Life Cycle"))
        reopenDic.Add("ApexSubType", wfi.DocProps("Reopen Apex Sub Type"))
        reopenDic.Add("PresentPosition", wfi.DocProps("Reopen Present Position"))
        reopenDic.Add("ChangeReason", wfi.DocProps("Reopen Code"))


        Return GelReopenXml(caseDic, workflowDic, reopenDic)
    End Function

    Private Function GetAbortXml(ByRef wfi As RDWFInstance, ByRef approvedBy As String, ByRef creator As String, ByRef comment As String) As XmlDocument
        Dim xml As New System.Xml.XmlDocument()
        Dim caseDic As New Dictionary(Of String, String)
        For Each key As String In GetCaseMapDic().Keys
            If key = "Modification Reason" Then
                If wfi.DocProps("Modification Code") = "IR" Then
                    caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
                Else
                    caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps("Other Modification Reason"))
                End If

            Else
                caseDic.Add(GetCaseMapDic().Item(key), wfi.DocProps(key))
            End If

        Next

        Dim reserveDic As New Dictionary(Of String, String)
        For Each newReservekey As String In reserveXmlTag
            If wfi.DocProps(newReservekey) <> wfi.DocProps(newReservekey + " (Original)") Then
                reserveDic.Add(newReservekey, wfi.DocProps(newReservekey + " (Adjust Amount)"))
            End If
        Next

        Dim workflowDic As New Dictionary(Of String, String)
        workflowDic.Add("eventId", wfi.DocProps("Event ID"))
        workflowDic.Add("creator", creator)
        workflowDic.Add("approvedBy", approvedBy)
        workflowDic.Add("assignedToUser", "")
        workflowDic.Add("action", "abort")
        workflowDic.Add("comment", comment)
        Return GelXml(caseDic, reserveDic, workflowDic)
    End Function

    Public Sub CallDodo(ByRef wfi As RDWFInstance, ByRef srv As DMSServer, ByRef xmlDoc As XmlDocument)
        srv.NewException(LogManager.LogLevel.Exception, "Input XML:" + xmlDoc.OuterXml)
        ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(Function() True)
        Dim client As New WebClient
        client.Headers.Add("Content-Type", "application/xml")
        Dim sentXml As Byte() = System.Text.Encoding.ASCII.GetBytes(xmlDoc.OuterXml)
        Dim responseByte As Byte() = client.UploadData(apiURL, "POST", sentXml)
        Dim responseString As String = System.Text.Encoding.ASCII.GetString(responseByte)
        Dim responseXml As XmlDocument = New XmlDocument
        responseXml.LoadXml(responseString)
        srv.NewException(LogManager.LogLevel.Exception, "OutPut Xml:" + responseString)
        If Not responseXml.LastChild.FirstChild.InnerText = "true" Then
            If responseXml.LastChild.ChildNodes.Item(1).Name = "message" Then
                Throw New Exception("API Error:" + responseXml.LastChild.ChildNodes.Item(1).InnerText)
            End If
            Throw New Exception("Error")
        End If

        If Not responseXml.LastChild.ChildNodes.Item(1).InnerText = "Successfully!" Then
            Throw New Exception("Error")
        End If

        If String.IsNullOrEmpty(wfi.DocProps("Event ID")) Then

            wfi.DocProps("Event ID") = responseXml.LastChild.LastChild.InnerText
        End If
    End Sub


    Public Sub Process(wfi As DMS.CoreAPI.RDWFInstance, srv As DMS.CoreAPI.DMSServer) Implements DMS.CoreAPI.IWFScriptTask.Process

        Try
            If Not String.IsNullOrEmpty(srv.GetCustomSetting("Reserve API URL")) Then
                apiURL = srv.GetCustomSetting("Reserve API URL")
            End If
            Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
            wfi.DocProps("Error") = ""
            Dim lastTaskInfo As Dictionary(Of String, String) = GlobalFunction.GetFinalResponseResult(wfi)
            If String.IsNullOrEmpty(lastTaskInfo.Item("result")) Then
                Throw New Exception("Last Result Not Found")
            End If
            Dim lastComment As String = ""
            Dim eformDoc As XmlDocument = GlobalFunction.GetEformXmlDocByWorkflow(wfi)
            Dim firstApprover As String = ""
            If Not eformDoc Is Nothing Then
                lastComment = GlobalFunction.GetLastComment(eformDoc)

                Dim parts As String() = GlobalFunction.GetDataByIdAndTag(eformDoc, "Combo", "First Approver").Split(New String() {"||"}, StringSplitOptions.RemoveEmptyEntries)
                If parts.Length > 0 Then
                    If IsNumeric(parts(0)) Then
                        Dim user As RDUser = uc.GetUserById(CLng(parts(0)))
                        firstApprover = user.Name
                    End If
                End If


            End If



            Dim xmlDoc As XmlDocument = Nothing
            If lastTaskInfo.Item("result") = "Submit" Then
                If lastTaskInfo.Item("taskName") = "Start" Then
                    xmlDoc = GetSubmitXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment, firstApprover, srv)
                Else
                    xmlDoc = GetSubmitReopenXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment, firstApprover, srv)
                End If
            ElseIf lastTaskInfo.Item("result") = "Abort" And Not String.IsNullOrEmpty(wfi.DocProps("Event ID")) Then
                xmlDoc = GetAbortXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment)
            ElseIf lastTaskInfo.Item("result") = "Reject" Then
                xmlDoc = GetRejectXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment)
            ElseIf lastTaskInfo.Item("result") = "Approve" Then
                xmlDoc = GetFinalXml(wfi, lastTaskInfo.Item("recipientName"), lastTaskInfo.Item("perviouRecipientName"), lastComment)
            End If

            CallDodo(wfi, srv, xmlDoc)

        Catch ex As Exception
            'wfi.StatusMsg = ex.Message
            wfi.DocProps("Error") = ex.Message
        End Try




    End Sub


End Class
