﻿Imports DMS.CoreAPI
Imports System.Xml

Public Class ToCloseAPI
    Implements IWFScriptTask
    Dim apiURL As String = "https://ewim.trm.com.hk/TRM/dodo/ApexAntelopeToDodo"
    Dim caseXmlTag() As String = {"CaseId", "ApexAction", "ClosureCode", "ApexSubType", "ClaimsLifeCycle"}
    Dim workflowXmlTag() As String = {"Creator", "Action", "Approver", "ChangeReason"}

    Public ReadOnly Property TaskId As String Implements IWFScriptTask.TaskId
        Get
            Return "ToCloseAPI"
        End Get
    End Property

    Private Function GetApproveXml(ByRef wfi As RDWFInstance, ByRef Approver As String) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toClose")
        CaseDic.Add("ClosureCode", wfi.DocProps("REQUEST_Closure_Reopen_Code"))
        CaseDic.Add("ApexSubType", wfi.DocProps("REQUEST_APEX_SUB_TYPE"))
        CaseDic.Add("ClaimsLifeCycle", wfi.DocProps("Claims Life Cycle"))

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Final")
        WorkflowDic.Add("Approver", Approver)
        WorkflowDic.Add("ChangeReason", wfi.DocProps("REQUEST_Closure_Reopen_Code"))
        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Private Function GetAbortXml(ByRef wfi As RDWFInstance) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toClose")

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Abort")
        WorkflowDic.Add("ChangeReason", "Abort for ApexToClose")
        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Public Sub Process(wfi As DMS.CoreAPI.RDWFInstance, srv As DMS.CoreAPI.DMSServer) Implements DMS.CoreAPI.IWFScriptTask.Process
        Try
            Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
            Dim lastTaskInfo As Dictionary(Of String, String) = GlobalFunction.GetFinalResponseResult(wfi)
            Dim xmlDoc As XmlDocument = Nothing
            Dim Approver As String = "Approver"
            If String.IsNullOrEmpty(lastTaskInfo.Item("result")) Then
                Throw New Exception("Last Result Not Found")
            End If

            If lastTaskInfo.Item("result") = "Approve" Then
                xmlDoc = GetApproveXml(wfi, ApexFunction.GetADName(wfi, uc, Approver))
            ElseIf lastTaskInfo.Item("result") = "Abort" Then
                xmlDoc = GetAbortXml(wfi)
            End If

            ApexFunction.CallDodo(apiURL, wfi, srv, xmlDoc)

        Catch ex As Exception
            wfi.DocProps("Error") = ex.Message
        End Try
    End Sub
End Class
