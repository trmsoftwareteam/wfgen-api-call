﻿Imports DMS.CoreAPI
Imports System.Xml

Public Class ToHRAPI
    Implements IWFScriptTask
    Dim apiURL As String = "https://ewim.trm.com.hk/TRM/dodo/ApexAntelopeToDodo"
    Dim caseXmlTag() As String = {"CaseId", "ApexAction", "RC", "CoRC", "RCStatus", "PresentPosition", "ApexSubType", "ClaimsLifeCycle"}
    Dim workflowXmlTag() As String = {"Creator", "Action", "Approver", "ChangeReason"}
    Public ReadOnly Property TaskId As String Implements IWFScriptTask.TaskId
        Get
            Return "ToHRAPI"
        End Get
    End Property

    Private Function GetApproveXml(ByRef wfi As RDWFInstance, ByRef Approver As String, ByRef RC As String, ByRef CoRC As String) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toHr")
        CaseDic.Add("RC", RC)
        CaseDic.Add("CoRC", CoRC)
        CaseDic.Add("RCStatus", wfi.DocProps("REQUEST_RC_Status"))
        CaseDic.Add("PresentPosition", wfi.DocProps("REQUEST_Present_Position"))
        CaseDic.Add("ApexSubType", wfi.DocProps("Apex Sub-Type"))
        CaseDic.Add("ClaimsLifeCycle", wfi.DocProps("REQUEST_Claims_Life_Cycle"))

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Approve")
        WorkflowDic.Add("Approver", Approver)
        WorkflowDic.Add("ChangeReason", wfi.DocProps("REQUEST_Amend_Reason"))
        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Private Function GetFinalXml(ByRef wfi As RDWFInstance, ByRef Approver As String) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toHr")
        CaseDic.Add("RCStatus", wfi.DocProps("CASE_RC_Status"))
        CaseDic.Add("PresentPosition", wfi.DocProps("CASE_Present_Position"))
        CaseDic.Add("ApexSubType", wfi.DocProps("Apex Sub-Type"))
        CaseDic.Add("ClaimsLifeCycle", wfi.DocProps("CASE_Claims_Life_Cycle"))

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Final")
        WorkflowDic.Add("Approver", Approver)
        WorkflowDic.Add("ChangeReason", wfi.DocProps("REQUEST_Amend_Reason"))
        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Private Function GetAbortXml(ByRef wfi As RDWFInstance) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toHr")

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Abort")
        WorkflowDic.Add("ChangeReason", "Abort for ApexToHR")
        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Public Sub Process(wfi As DMS.CoreAPI.RDWFInstance, srv As DMS.CoreAPI.DMSServer) Implements DMS.CoreAPI.IWFScriptTask.Process
        Try
            Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
            Dim lastTaskInfo As Dictionary(Of String, String) = GlobalFunction.GetFinalResponseResult(wfi)
            Dim xmlDoc As XmlDocument = Nothing
            Dim Approver As String = "Approver"
            Dim RC As String = "RC Handler Assign"
            Dim CoRC As String = "Co RC Assign"

            If String.IsNullOrEmpty(lastTaskInfo.Item("result")) Then
                Throw New Exception("Last Result Not Found")
            End If

            If lastTaskInfo.Item("result") = "Approve" Then
                xmlDoc = GetApproveXml(wfi, ApexFunction.GetADName(wfi, uc, Approver), ApexFunction.GetADName(wfi, uc, RC), ApexFunction.GetADName(wfi, uc, CoRC))
            ElseIf lastTaskInfo.Item("result") = "Accept" Then
                xmlDoc = GetFinalXml(wfi, ApexFunction.GetADName(wfi, uc, Approver))
            ElseIf lastTaskInfo.Item("result") = "Abort" Then
                xmlDoc = GetAbortXml(wfi)
            End If

            ApexFunction.CallDodo(apiURL, wfi, srv, xmlDoc)

        Catch ex As Exception
            wfi.DocProps("Error") = ex.Message
        End Try
    End Sub
End Class
