﻿Imports DMS.CoreAPI
Imports System.Xml
Public Class ToLitigatedAPI
    Implements IWFScriptTask
    Dim apiURL As String = "https://ewim.trm.com.hk/TRM/dodo/ApexAntelopeToDodo"
    Dim caseXmlTag() As String = {"CaseId", "ApexAction", "RC", "ApexSubType", "ClaimsLifeCycle", "PanelSolicitor", "MType", "CMAction", "CMStaff", "JobType", "JobStaff", "JobCompletedDate", "JobDueDate", "ECPreactionDate", "CLPreactionDate", "ECApplicationNo", "WritNo", "EC1stHearingDate", "WritServiceDate", "ClaimantsSolicitors"}
    Dim workflowXmlTag() As String = {"Creator", "Action", "Approver", "ChangeReason"}

    Public ReadOnly Property TaskId As String Implements IWFScriptTask.TaskId
        Get
            Return "ToLitigatedAPI"
        End Get
    End Property
    Private Function GetApproveXml(ByRef wfi As RDWFInstance, ByRef Action As String, ByRef Approver As String, ByRef RC As String) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toLitigated")
        CaseDic.Add("RC", RC)
        CaseDic.Add("ApexSubType", "New Litigated to be reviewed")
        CaseDic.Add("ClaimsLifeCycle", "New Litigated to be reviewed")
        CaseDic.Add("PanelSolicitor", wfi.DocProps("APPROVAL_Solicitor_Assigned"))
        CaseDic.Add("JobCompletedDate", wfi.DocProps("Job Complete Date"))
        CaseDic.Add("JobDueDate", wfi.DocProps("Job Due Date"))
        CaseDic.Add("ECPreactionDate", wfi.DocProps("EC Preaction Date"))
        CaseDic.Add("CLPreactionDate", wfi.DocProps("CL Preaction Date"))
        CaseDic.Add("ECApplicationNo", wfi.DocProps("EC Application No"))
        CaseDic.Add("WritNo", wfi.DocProps("Writ No"))
        CaseDic.Add("EC1stHearingDate", wfi.DocProps("EC 1st Hearing Date"))
        CaseDic.Add("WritServiceDate", wfi.DocProps("Writ Service Date"))
        CaseDic.Add("ClaimantsSolicitors", wfi.DocProps("Claimants Solicitor"))


        If wfi.DocProps("APPROVAL_Handler_Assigned") = "Winnie Lam" Then
            CaseDic.Add("MType", "Apex-DS-LH")
            CaseDic.Add("CMAction", "Legal Case. Assign legal panel for further handling")
            CaseDic.Add("CMStaff", "sun.lai")
            CaseDic.Add("JobType", "Apex-DS-LH")
            CaseDic.Add("JobStaff", "winnie.lam")
        End If

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", Action)
        WorkflowDic.Add("Approver", Approver)
        WorkflowDic.Add("ChangeReason", wfi.DocProps("REQUEST_Amend_Reason"))

        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Private Function GetSubmitXml(ByRef wfi As RDWFInstance, ByRef Approver As String, ByRef RC As String, ByRef JobStaff As String) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toLitigated")
        CaseDic.Add("RC", RC)
        CaseDic.Add("ApexSubType", "New Litigated to be reviewed")
        CaseDic.Add("ClaimsLifeCycle", "New Litigated to be reviewed")
        CaseDic.Add("PanelSolicitor", wfi.DocProps("APPROVAL_Solicitor_Assigned"))
        CaseDic.Add("JobCompletedDate", wfi.DocProps("Job Complete Date"))
        CaseDic.Add("JobDueDate", wfi.DocProps("Job Complete Date"))

        CaseDic.Add("MType", wfi.DocProps("CASE_Job_Type"))
        CaseDic.Add("CMAction", "Insurer apporves DS Legal")
        CaseDic.Add("CMStaff", "sun.lai")
        CaseDic.Add("JobType", wfi.DocProps("CASE_Job_Type"))
        CaseDic.Add("JobStaff", JobStaff)

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Approve3_Add_Job")
        WorkflowDic.Add("Approver", Approver)
        WorkflowDic.Add("ChangeReason", wfi.DocProps("REQUEST_Amend_Reason"))

        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Private Function GetConfirmXml(ByRef wfi As RDWFInstance, ByRef Approver As String) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toLitigated")
        CaseDic.Add("ApexSubType", wfi.DocProps("JOB_Apex_Sub_Type"))
        CaseDic.Add("ClaimsLifeCycle", wfi.DocProps("JOB_Claims_Life_Cycle"))
        CaseDic.Add("PanelSolicitor", wfi.DocProps("JOB_Solicitor_Assigned"))

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Final4")
        WorkflowDic.Add("Approver", Approver)
        WorkflowDic.Add("ChangeReason", wfi.DocProps("REQUEST_Amend_Reason"))

        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function

    Private Function GetAbortXml(ByRef wfi As RDWFInstance) As XmlDocument
        Dim CaseDic As New Dictionary(Of String, String)
        CaseDic.Add("CaseId", wfi.DocProps("Case ID"))
        CaseDic.Add("ApexAction", "toLitigated")

        Dim WorkflowDic As New Dictionary(Of String, String)
        WorkflowDic.Add("Creator", wfi.Owner.Name)
        WorkflowDic.Add("Action", "Abort")
        WorkflowDic.Add("ChangeReason", wfi.DocProps("REQUEST_Amend_Reason"))

        Return ApexFunction.GetApexXml(caseXmlTag, workflowXmlTag, CaseDic, WorkflowDic)
    End Function
    Public Sub Process(wfi As DMS.CoreAPI.RDWFInstance, srv As DMS.CoreAPI.DMSServer) Implements DMS.CoreAPI.IWFScriptTask.Process
        Try
            Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
            Dim lastTaskInfo As Dictionary(Of String, String) = GlobalFunction.GetFinalResponseResult(wfi)
            Dim xmlDoc As XmlDocument = Nothing
            Dim Approver As String = "Case Assigner"
            Dim RC As String = "RC Handler Assign"
            Dim JobStaff As String = "Job Handler_TRM"

            If String.IsNullOrEmpty(lastTaskInfo.Item("result")) Then
                Throw New Exception("Last Result Not Found")
            End If

            If lastTaskInfo.Item("result") = "Approve" Then
                If wfi.DocProps("APPROVAL_Team_Assigned") = "SR" Then
                    xmlDoc = GetApproveXml(wfi, "Final2", ApexFunction.GetADName(wfi, uc, Approver), ApexFunction.GetADName(wfi, uc, RC))
                ElseIf wfi.DocProps("APPROVAL_Handler_Assigned") = "Chloe Tang" Then
                    xmlDoc = GetApproveXml(wfi, "Approve2", ApexFunction.GetADName(wfi, uc, Approver), ApexFunction.GetADName(wfi, uc, RC))
                ElseIf wfi.DocProps("APPROVAL_Handler_Assigned") = "Winnie Lam" Then
                    xmlDoc = GetApproveXml(wfi, "Approve2_Add_Job", ApexFunction.GetADName(wfi, uc, Approver), ApexFunction.GetADName(wfi, uc, RC))
                End If
            ElseIf lastTaskInfo.Item("taskName") = "Legal Team Manager" Then
                xmlDoc = GetSubmitXml(wfi, ApexFunction.GetADName(wfi, uc, Approver), ApexFunction.GetADName(wfi, uc, RC), ApexFunction.GetADName(wfi, uc, JobStaff))
            ElseIf lastTaskInfo.Item("taskName") = "Legal Job Handler" Or lastTaskInfo.Item("taskName") = "Litigated Team Manager" Or lastTaskInfo.Item("taskName") = "SR Handler" Then
                xmlDoc = GetConfirmXml(wfi, ApexFunction.GetADName(wfi, uc, Approver))
            ElseIf lastTaskInfo.Item("result") = "Abort" Then
                xmlDoc = GetAbortXml(wfi)
            End If

            ApexFunction.CallDodo(apiURL, wfi, srv, xmlDoc)

        Catch ex As Exception
            wfi.DocProps("Error") = ex.Message
        End Try
    End Sub
End Class
