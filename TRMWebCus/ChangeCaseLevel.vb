﻿Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml
Public Class ChangeCaseLevel
    Implements AntelopeWebPlugin.IPluginCustomService
    Dim caseNumberfId As String = "247"
    Dim basicLevelNumberfId As String = "307"
    Dim highLevelNumberfId As String = "308"
    Dim levelfId As String = "236"

    Dim _basicLevelNumber As String = ""
    Dim _highLevelNumber As String = ""

    Public ReadOnly Property FuncId As String Implements AntelopeWebPlugin.IPlugin.FuncId
        Get
            Return "ChangeCaseLevel"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMS.CoreAPI.DMSServer, parentId As String, id As String, props As System.Collections.Generic.Dictionary(Of String, String)) As System.Xml.XmlNode Implements AntelopeWebPlugin.IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()
        Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
        xmlDocument.AppendChild(resultNode)
        Try
            Dim caseNumber As String = props.Item("CaseNumber")
            _basicLevelNumber = props.Item("BasicLevelNumber")
            _highLevelNumber = props.Item("HighLevelNumber")
            ErrorCheck(caseNumber, _basicLevelNumber, _highLevelNumber)
            Dim rdEx As New RDExpression(srv)
            rdEx.AddExp(New RDCriteria(srv, "Field" + caseNumberfId, FieldType.NVarchar, "=", caseNumber), "and")

            Dim fc As RDFolderController = srv.CreateController(ControllerType.RDFolderController)
            Dim caseFolder As RDFolder = fc.GetFolderByPath("TRM\Case")

            Dim rdCollection As RDCollection = GlobalFunction.QueryByRDExpression(srv, caseFolder, rdEx, 99999, "")
            If Not rdCollection Is Nothing Then
                For Each rdSuc As RDSecuObject In rdCollection
                    ChangeIndex(rdSuc)

                Next
            End If

        Catch ex As Exception
            resultNode.InnerText = "Error:" + ex.Message
        End Try

        Return xmlDocument
    End Function

    Public Sub ErrorCheck(ByRef caseNumber As String, ByRef basicLevelNumber As String, ByRef highLevelNumber As String)
        If String.IsNullOrEmpty(caseNumber) Then
            Throw New Exception("Case Number Is Null")
        End If

        If String.IsNullOrEmpty(basicLevelNumber) And String.IsNullOrEmpty(highLevelNumber) Then
            Throw New Exception("Level Number Is Null")
        End If

        If Not String.IsNullOrEmpty(basicLevelNumber) And Not String.IsNullOrEmpty(highLevelNumber) Then
            Throw New Exception("Base Level Number and high Level Number cannot exist at the same time")
        End If
    End Sub

    Public Sub ChangeIndex(ByRef rdSuc As RDSecuObject)
        If rdSuc.SecuType = RDSecuObject.RDSecuType.RDFolder Then
            Dim rdFolder As RDFolder = rdSuc
            If Not String.IsNullOrEmpty(_basicLevelNumber) Then
                rdFolder.DocProps(CLng(levelfId)) = "Basic"
                rdFolder.DocProps(CLng(basicLevelNumberfId)) = _basicLevelNumber
                rdFolder.Update()
            End If

            If Not String.IsNullOrEmpty(_highLevelNumber) Then
                rdFolder.DocProps(CLng(levelfId)) = "High"
                rdFolder.DocProps(CLng(highLevelNumberfId)) = _highLevelNumber
                rdFolder.Update()
            End If
        End If


        If rdSuc.SecuType = RDSecuObject.RDSecuType.RDDocument Then
            Dim rdDoc As RDDocument = rdSuc
            If Not String.IsNullOrEmpty(_basicLevelNumber) Then
                rdDoc.DocProps(CLng(levelfId)) = "Basic"
                rdDoc.DocProps(CLng(basicLevelNumberfId)) = _basicLevelNumber
                rdDoc.Update()
            End If

            If Not String.IsNullOrEmpty(_highLevelNumber) Then
                rdDoc.DocProps(CLng(levelfId)) = "High"
                rdDoc.DocProps(CLng(highLevelNumberfId)) = _highLevelNumber
                rdDoc.Update()
            End If
        End If

    End Sub
End Class
