﻿Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml
Public Class CreateCase
    Implements AntelopeWebPlugin.IPluginCustomService
    'Dim _caseNumber As String = ""
    'Dim _customerName As String = ""
    Dim fullControlPid As Long = 1
    Dim readOnlyPid As Long = 2

    Dim BLMNo As String
    Dim HLMNo As String
    Dim InjuredName As String
    Dim HKID As String
    Dim CaseStatus As String
    Dim DOA As String
    Dim DOR As String
    Dim Insurer As String
    Dim Insured As String
    Dim Description As String

    Public ReadOnly Property FuncId As String Implements AntelopeWebPlugin.IPlugin.FuncId
        Get
            Return "CreateCase"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMS.CoreAPI.DMSServer, parentId As String, id As String, props As System.Collections.Generic.Dictionary(Of String, String)) As System.Xml.XmlNode Implements AntelopeWebPlugin.IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()
        Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
        xmlDocument.AppendChild(resultNode)
        srv.NewException(LogManager.LogLevel.Exception, "CreateCase")
        Try
            Dim parentCompanyCode As String = props.Item("ParentCompanyCode")
            Dim subsidiaryCompanyCode As String = props.Item("SubsidiaryCompanyCode")
            Dim insurerCompanyCode As String = props.Item("InsurerCompanyCode")

            BLMNo = GlobalFunction.GetSalfString(props, "BLMNo")
            HLMNo = GlobalFunction.GetSalfString(props, "HLMNo")
            InjuredName = GlobalFunction.GetSalfString(props, "InjuredName")
            HKID = GlobalFunction.GetSalfString(props, "HKID")
            CaseStatus = GlobalFunction.GetSalfString(props, "CaseStatus")
            DOA = GlobalFunction.GetSalfString(props, "DOA")
            DOR = GlobalFunction.GetSalfString(props, "DOR")
            Insurer = GlobalFunction.GetSalfString(props, "Insurer")
            Insured = GlobalFunction.GetSalfString(props, "Insured")
            Description = GlobalFunction.GetSalfString(props, "Description")

            If String.IsNullOrEmpty(BLMNo) Then
                Throw New Exception("Case Number Is Null")
            End If

            If String.IsNullOrEmpty(InjuredName) Then
                Throw New Exception("Case Number Is Null")
            End If


            If Not String.IsNullOrEmpty(subsidiaryCompanyCode) Then
                parentCompanyCode = parentCompanyCode + "," + subsidiaryCompanyCode
            End If

            Dim fc As RDFolderController = srv.CreateController(ControllerType.RDFolderController)
            Dim rdFolder As RDFolder = fc.GetFolderByPath("Home\Case", True)
            SetPermission(srv, rdFolder, parentCompanyCode, readOnlyPid)
            SetPermission(srv, rdFolder, insurerCompanyCode, readOnlyPid)
            SetPermission(srv, rdFolder, "TRM Staff", readOnlyPid)
            SetPermission(srv, rdFolder, "TRM Admin", fullControlPid)
            rdFolder.Update()

            Dim caseFolder As RDFolder = CreateFolderAndSetPermission(srv, "Home\Case\" + BLMNo + "-" + InjuredName, parentCompanyCode, insurerCompanyCode)
            Dim workflowFolder As RDFolder = CreateFolderAndSetPermission(srv, "Home\Case\" + BLMNo + "-" + InjuredName + "\Workflow", parentCompanyCode, insurerCompanyCode)

            'Dim trmFolder As RDFolder = CreateFolderAndSetPermission(srv, "TRM", parentCompanyCode, insurerCompanyCode)
            
            'Dim caseFolder As RDFolder = CreateFolderAndSetPermission(srv, "TRM\Case\" + _caseNumber + " folder", parentCompanyCode, insurerCompanyCode)


            'Dim claimsFolder As RDFolder = CreateFolderAndSetPermission(srv, "TRM\Case\" + _caseNumber + " folder" + "\claims", parentCompanyCode, "")


            'Dim insurerFolder As RDFolder = CreateFolderAndSetPermission(srv, "TRM\Case\" + _caseNumber + " folder" + "\insurer", insurerCompanyCode, "")


            'Dim staffFolder As RDFolder = CreateFolderAndSetPermission(srv, "TRM\Case\" + _caseNumber + " folder" + "\staff", "", "")

            'Dim emailFolder As RDFolder = CreateFolderAndSetPermission(srv, "TRM\Case\" + _caseNumber + " folder" + "\staff\email", "", "")
            resultNode.InnerText = "Success"
        Catch ex As Exception
            resultNode.InnerText = "Error:" + ex.Message
        End Try

        Return xmlDocument
    End Function

    Private Function CreateFolderAndSetPermission(ByRef srv As DMSServer, ByRef folderPath As String, ByRef employerRoles As String, ByRef insurerRoles As String) As RDFolder
        Dim fc As RDFolderController = srv.CreateController(ControllerType.RDFolderController)

        Dim rdFolder As RDFolder = fc.GetFolderByPath(folderPath, True)
        rdFolder.IsPropagateIndex = True

        If rdFolder.CheckAffected Then
            Dim op As New RDCopyACLOperation(srv, rdFolder.ID)
            op.Update()
        End If
        rdFolder.CopyACL()

        rdFolder.RemoveAllNonSystemACL()

        SetPermission(srv, rdFolder, employerRoles, readOnlyPid)

        SetPermission(srv, rdFolder, insurerRoles, readOnlyPid)

        SetPermission(srv, rdFolder, "TRM Staff", readOnlyPid)
        SetPermission(srv, rdFolder, "TRM Admin", fullControlPid)

        rdFolder.DocProps("BLM No") = BLMNo
        rdFolder.DocProps("HLM No") = HLMNo
        rdFolder.DocProps("Injured Name") = InjuredName
        rdFolder.DocProps("HKID") = HKID
        rdFolder.DocProps("Case Status") = CaseStatus
        rdFolder.DocProps("DOA") = DOA
        rdFolder.DocProps("DOR") = DOR
        rdFolder.DocProps("Insurer") = Insurer
        rdFolder.DocProps("Insured") = Insured
        rdFolder.DocProps("Description") = Description

        rdFolder.Update()

        Return rdFolder
    End Function

    Private Sub SetPermission(ByRef srv As DMSServer, ByRef rdFolder As RDFolder, ByRef userRoles As String, ByRef permissionId As Long)
         If Not String.IsNullOrEmpty(userRoles) Then

            For Each userRole As String In userRoles.Split(",")
                userRole = userRole.Trim()
                If Not String.IsNullOrEmpty(userRole) Then
                    Dim rdRode As RDRole = GlobalFunction.GetRoleByName(srv, userRole)
                    GlobalFunction.AddPermission(srv, rdFolder, rdRode, permissionId)
                End If
            Next
        End If


    End Sub

End Class
