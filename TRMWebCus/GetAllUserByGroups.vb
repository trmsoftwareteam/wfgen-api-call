﻿Imports AntelopeWebPlugin
Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml


Public Class GetAllUserByGroups
    Implements AntelopeWebPlugin.IPluginCustomService

    Public ReadOnly Property FuncId As String Implements IPlugin.FuncId
        Get
            Return "GetAllUserByGroups"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMSServer, parentId As String, id As String, props As Dictionary(Of String, String)) As XmlNode Implements IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()
        Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
        xmlDocument.AppendChild(resultNode)
        Try
            If String.IsNullOrEmpty(props("userGroups")) Then
                Throw New Exception("")
            End If

            Return GetUsers(srv, props("userGroups")).ToXml
        Catch ex As Exception
            srv.NewException(3, "ex:" + ex.Message)
        End Try
        Return xmlDocument
    End Function

    Public Function GetUsers(ByRef srv As DMS.CoreAPI.DMSServer, ByRef groups As String) As RDCollection
        Dim groupNameList As New List(Of String)

        For Each groupName As String In groups.Split(",")
            If Not String.IsNullOrEmpty(groupName) Then
                groupNameList.Add(groupName)
            End If

        Next



        Dim rdCollection As New RDCollection(srv)
        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        For Each groupName As String In groupNameList
            Dim rdRole As RDRole = uc.GetRoleByName(groupName)
            If Not rdRole Is Nothing Then
                For Each rdUser As RDUser In rdRole.GetAllUsers(True)
                    Dim isExist As Boolean = False

                    For Each existUser As RDUser In rdCollection
                        If rdUser.ID = existUser.ID Then
                            isExist = True
                        End If
                    Next

                    If Not isExist Then
                        rdCollection.Add(rdUser)
                    End If


                Next
            End If


        Next

        Return rdCollection
    End Function
End Class
