﻿Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml
Imports WFTemplate
Public Class GetWorkflowHistory
    Implements AntelopeWebPlugin.IPluginCustomService

    Public ReadOnly Property FuncId As String Implements AntelopeWebPlugin.IPlugin.FuncId
        Get
            Return "GetWorkflowHistory"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMS.CoreAPI.DMSServer, parentId As String, id As String, props As System.Collections.Generic.Dictionary(Of String, String)) As System.Xml.XmlNode Implements AntelopeWebPlugin.IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()
        Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
        xmlDocument.AppendChild(resultNode)
        srv.NewException(LogManager.LogLevel.Exception, "GetWorkflowHistory")
        srv.NewException(LogManager.LogLevel.Exception, "props:" + String.Join(",", props.Select(Function(pair) String.Format("{0}={1}", pair.Key, pair.Value)).ToArray()))

        Try
            Dim wiId As Long = CLng(props.Item("wiId"))
            srv.NewException(LogManager.LogLevel.Exception, "wiId:" + props.Item("wiId"))
            Dim wc As RDWFInstanceController = srv.CreateController(ControllerType.RDWFInstanceController)
            Dim wfi As RDWFInstance = wc.GetWFInstanceById(wiId)
            srv.NewException(LogManager.LogLevel.Exception, "GetWorkflowHistory1")
            Dim wfTemplate As WFTemplate.WFTemplate = wfi.GetTemplate()
            Dim taskList As RDCollection = wfi.TasksList
            Dim allComments As RDCollection = wfi.GetAllComments()
            srv.NewException(LogManager.LogLevel.Exception, "GetWorkflowHistory2")
            Dim lastComment As String = ""
            For Each rdTask As RDWFInstanceTask In taskList
                srv.NewException(LogManager.LogLevel.Exception, "GetWorkflowHistory3")
                srv.NewException(LogManager.LogLevel.Exception, "GetWorkflowHistory4:" + rdTask.Recipients.Count.ToString)
                If rdTask.Recipients.Count <> 0 Then
                    Dim taskNode As XmlNode = xmlDocument.CreateElement("Task")

                    Dim witIdAttr As XmlAttribute = xmlDocument.CreateAttribute("witId")
                    witIdAttr.Value = rdTask.ID.ToString
                    taskNode.Attributes.Append(witIdAttr)


                    Dim workflowNameAttr As XmlAttribute = xmlDocument.CreateAttribute("workflowName")
                    If wfi.Name.Contains("Leave Application - RC") Then
                        workflowNameAttr.Value = "Leave Application - RC"
                    ElseIf wfi.Name.Contains("New RC case") Then
                        workflowNameAttr.Value = "New RC case"
                    Else
                        workflowNameAttr.Value = wfi.Name
                    End If

                    taskNode.Attributes.Append(workflowNameAttr)

                    Dim workflowStatusAttr As XmlAttribute = xmlDocument.CreateAttribute("workflowStatus")
                    workflowStatusAttr.Value = wfi.Status.ToString
                    taskNode.Attributes.Append(workflowStatusAttr)

                    Dim requestByAttr As XmlAttribute = xmlDocument.CreateAttribute("requestBy")
                    requestByAttr.Value = wfi.Owner.DescOrName
                    taskNode.Attributes.Append(requestByAttr)

                    Dim toBeDoneByAttr As XmlAttribute = xmlDocument.CreateAttribute("toBeDoneBy")
                    srv.NewException(LogManager.LogLevel.Exception, "Recipients:" + rdTask.Recipients.Count.ToString)
                    Dim rdWFInstanceTaskRecipient As RDWFInstanceTaskRecipient = Nothing
                    For Each recipent As RDWFInstanceTaskRecipient In rdTask.Recipients
                        If Not String.IsNullOrEmpty(recipent.ReadDateStr) Then
                            rdWFInstanceTaskRecipient = recipent
                            Exit For
                        End If
                    Next

                    If rdWFInstanceTaskRecipient Is Nothing Then
                        rdWFInstanceTaskRecipient = rdTask.Recipients(0)
                    End If




                    toBeDoneByAttr.Value = rdWFInstanceTaskRecipient.User.DescOrName
                    taskNode.Attributes.Append(toBeDoneByAttr)

                    Dim assignDateAttr As XmlAttribute = xmlDocument.CreateAttribute("assignDate")
                    assignDateAttr.Value = wfi.CreateDate.ToString("yyyy-MM-dd hh:mm:ss")
                    taskNode.Attributes.Append(assignDateAttr)

                    Dim dueDateAttr As XmlAttribute = xmlDocument.CreateAttribute("dueDate")
                    srv.NewException(LogManager.LogLevel.Exception, "Log1:" + rdTask.TaskId.ToString)
                    srv.NewException(LogManager.LogLevel.Exception, "Log:" + GetDueDay(wfi, rdTask.TaskId).ToString)
                    dueDateAttr.Value = GlobalFunction.AddWorkDays(rdTask.StartDate, GetDueDay(wfi, rdTask.TaskId)).ToString("yyyy-MM-dd hh:mm:ss")
                    taskNode.Attributes.Append(dueDateAttr)

                    Dim responseDateAttr As XmlAttribute = xmlDocument.CreateAttribute("responseDate")
                    responseDateAttr.Value = rdTask.CompleteDateStr
                    taskNode.Attributes.Append(responseDateAttr)

                    Dim taskStatusAttr As XmlAttribute = xmlDocument.CreateAttribute("taskStatus")
                    If String.IsNullOrEmpty(rdWFInstanceTaskRecipient.Result) Then
                        taskStatusAttr.Value = "Waiting for response"
                    Else
                        taskStatusAttr.Value = rdWFInstanceTaskRecipient.Result
                    End If

                    taskNode.Attributes.Append(taskStatusAttr)

                    Dim commentAttr As XmlAttribute = xmlDocument.CreateAttribute("comment")
                    Dim comments As String = rdWFInstanceTaskRecipient.Comments
                    For Each c As RDComment In allComments
                        srv.NewException(LogManager.LogLevel.Exception, c.Parent.SecuType.ToString)
                        srv.NewException(LogManager.LogLevel.Exception, c.Parent.ID.ToString)
                        srv.NewException(LogManager.LogLevel.Exception, c.Parent.Name)
                        If c.RelatedId = rdWFInstanceTaskRecipient.ID Then
                            If comments <> "" Then
                                comments &= "<br/>"
                            End If
                            comments &= c.Comment
                            lastComment = c.Comment
                        End If
                    Next
                    commentAttr.Value = comments
                    taskNode.Attributes.Append(commentAttr)

                    resultNode.AppendChild(taskNode)
                End If


            Next

            wfi.DocProps("Latest Comment") = lastComment
            wfi.Update()

        Catch ex As Exception
            resultNode.InnerText = "Error:" + ex.Message
        End Try
        Return xmlDocument
    End Function

    Private Function GetDueDay(ByRef wfi As RDWFInstance, ByRef taskId As Long) As Integer
        Try
            Dim wfTask As WFTemplate.WFTask = wfi.GetTemplate.getTask(taskId)

            Dim day As Integer = wfTask.DeadLineHours / 24
            Return day
        Catch ex As Exception

        End Try
        
        Return (0)
    End Function
End Class
