﻿Imports System.IO
Imports DMS.CoreAPI
Imports System.Xml
Public Class GlobalFunction
    Public Shared Sub WriteToFile(text As String)
        Try
            Dim path As String = "C:\AntelopeCusLog" + "\AntelopeCusLog" + DateTime.Now.ToString("ddMMyyyy") + ".txt"
            If (Not System.IO.Directory.Exists("C:\AntelopeCusLog")) Then
                System.IO.Directory.CreateDirectory("C:\AntelopeCusLog")
            End If
            Using writer As New StreamWriter(path, True)
                writer.WriteLine(String.Format(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ": " + text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")))
                writer.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub


    Public Shared Function GetSalfString(ByRef dic As Dictionary(Of String, String), ByRef key As String)
        If String.IsNullOrEmpty(dic.Item(key)) Then
            Return ""
        Else
            Return dic.Item(key)
        End If
    End Function

    Public Shared Function AddWorkDays(ByVal startDate As Date, ByVal workDays As Integer) As Date
        Dim endDate As Date = startDate
        Try
            Dim n As Integer = 0
            If workDays > 0 Then
                n = 1
            ElseIf workDays < 0 Then
                n = -1
            End If
            If n <> 0 Then
                For i = 1 To Math.Abs(workDays)
                    endDate = endDate.AddDays(n)
                    While (endDate.DayOfWeek = DayOfWeek.Saturday OrElse endDate.DayOfWeek = DayOfWeek.Sunday)
                        endDate = endDate.AddDays(n)
                    End While
                Next
            End If
        Catch ex As Exception

        End Try
        
        Return endDate
    End Function

    Public Shared Function GetRoleByName(ByRef srv As DMSServer, ByRef roleName As String) As RDRole
        srv.NewException(LogManager.LogLevel.Exception, "roleName:" + roleName)

        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        Dim rdRole As RDRole = uc.GetRoleByName(roleName)
        If rdRole Is Nothing Then
            rdRole = uc.CreateRole(roleName)
            rdRole.IsAccess(RDSecurity.SystemSecurity.FolderAccess) = True
            rdRole.Update()
        End If
        Return rdRole
    End Function

    Public Shared Sub AddPermission(ByRef srv As DMSServer, ByRef rdfolder As RDFolder, ByRef rdRole As RDRole, ByRef permissionId As Long)
        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        Dim plc As RDPermissionController = srv.CreateController(ControllerType.RDPermissionController)
        Dim staffPl As RDPermissionLevel = plc.GetPermissionLevelById(permissionId)
        Dim acl As RDACL = rdfolder.UpdateACL(rdRole, staffPl)

    End Sub

    Public Shared Function GetUserByName(ByRef srv As DMSServer, ByRef userName As String) As RDUser
        srv.NewException(LogManager.LogLevel.Exception, "userName:" + userName)
        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        Dim rdUser As RDUser = uc.GetUserByName(userName)
        If rdUser Is Nothing Then
            rdUser = uc.CreateUser(userName)
            rdUser.Password = "pass@word1"
            rdUser.Update()
        End If
        Return rdUser
    End Function

    Public Shared Sub AddRole(ByRef srv As DMSServer, ByRef rdUser As RDUser, ByRef tragetRole As RDRole)
        Dim isExist As Boolean = False
        For Each rdRole As RDRole In rdUser.GetRoleList(True)
            If rdRole.ID = tragetRole.ID Then
                isExist = True
            End If
        Next

        If Not isExist Then
            rdUser.AssignUserToRole(tragetRole)
            rdUser.Update()
            tragetRole.Update()
        End If
    End Sub

    Public Shared Function QueryByRDExpression(ByRef srv As DMSServer, ByRef tragetFolder As RDSecuObject, ByRef rdEx As RDExpression, ByRef count As Integer, ByRef sortBy As String) As RDCollection
        Dim qc As RDQueryController = srv.CreateController(ControllerType.RDQueryController)
        Dim qp As New QueryParameter(0, count, sortBy, True, Nothing, False, rdEx, True, True)
        Dim result As RDCollection = qc.QueryObjects(tragetFolder, qp).Result
        If result Is Nothing Then
            Return Nothing
        Else
            If result.Count = 0 Then
                Return Nothing
            Else
                Return result
            End If
        End If
    End Function

    Public Shared Function GetEformDoc(ByRef wfi As RDWFInstance) As RDDocument
        Try
            For Each rdSecu As RDSecuObject In wfi.GetRelations
                If rdSecu.SecuType = RDSecuObject.RDSecuType.RDDocument Then
                    Dim rdDoc As RDDocument = rdSecu
                    Dim v As RDVersion = rdDoc.GetVersion
                    Dim ext As String = v.Extension.ToLower
                    If ext = ".rfti" Then
                        Return rdDoc
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
        Return Nothing
    End Function

    Public Shared Function GetEformXmlDocByWorkflow(ByRef wfi As RDWFInstance) As XmlDocument


        Dim xmlDoc As XmlDocument = Nothing
        Try
            WriteToFile("wfi:" + wfi.Name)
            WriteToFile("GetRelations:" + wfi.GetRelations.Count.ToString)
            For Each rdDoc As RDDocument In wfi.GetRelations
                Dim v As RDVersion = rdDoc.GetVersion
                Dim ext As String = v.Extension.ToLower
                WriteToFile("rfti:" + ext)
                If ext = ".rfti" Then
                    Dim eform As DMS.EForm.EForm = v.GetEFormContent
                    Return eform.GetXmlDocument()
                End If
            Next



        Catch ex As Exception
            WriteToFile("Err:" + ex.Message)
        End Try

        If xmlDoc Is Nothing Then
            Throw New Exception("GetEformXmlDocByWorkflow Error")
        Else
            Return xmlDoc
        End If
    End Function


    Public Shared Function GetElementByIdAndTag(ByRef xmlDoc As Xml.XmlDocument, ByRef id As String, ByRef tag As String) As Xml.XmlElement
        Dim xmlNodeList As Xml.XmlNodeList = xmlDoc.GetElementsByTagName(tag)

        For Each xmlElement As Xml.XmlElement In xmlNodeList
            For Each xmlAttribute As Xml.XmlAttribute In xmlElement.Attributes
                If xmlAttribute.Name.ToLower = "id" Then
                    If xmlAttribute.Value = id Then
                        Return xmlElement
                    End If
                End If
            Next
        Next

        Return Nothing
    End Function

    Public Shared Sub BindDataToIndex(ByRef _srv As DMSServer, ByRef newEform As RDDocument, ByRef newWfi As RDWFInstance)
        Dim nEform As DMS.EForm.EForm = newEform.GetVersion.GetEFormContent
        Dim formIndex As Dictionary(Of String, Object) = nEform.ExtractDatabinding()
        For Each key As String In formIndex.Keys
            If TypeOf formIndex(key) Is Data.DataTable Then
                Dim dt As Data.DataTable = formIndex(key)
                If Not newWfi.DetailProps(key) Is Nothing Then
                    newWfi.DetailProps(key).Sync(dt)
                End If
                If Not newEform.DetailProps(key) Is Nothing Then
                    newEform.DetailProps(key).Sync(dt)
                End If
            Else
                If Not key = "Sequence no." Then
                    newWfi.DocProps(key) = formIndex(key)
                    newEform.DocProps(key) = formIndex(key)
                End If

            End If
        Next
        If formIndex.Count > 0 Then
            newWfi.Update()
        End If
    End Sub

    Public Shared Function StartWorkflow(ByRef srv As DMSServer, ByRef template As RDDocument, ByRef props As Dictionary(Of String, String)) As Long
        Dim doclist As New RDCollection(srv, RDCollection.RDType.RDDocument)


        ' start the workflow
        Dim wfic As RDWFInstanceController = srv.CreateController(ControllerType.RDWFInstanceController)
        Dim wfi As RDWFInstance = wfic.CreateNewWFInstance(template, doclist, Nothing, Nothing, 50)


        For Each key As String In props.Keys
            If wfi.HasDocProps(key) Then
                srv.NewException(3, key + ":" + props.Item(key))
                wfi.DocProps(key) = props.Item(key)
            End If
        Next



        Dim rdWFInstanceTask As RDWFInstanceTask = wfi.ActiveTasks(0)

        Dim rdWFInstanceTaskRecipient As RDWFInstanceTaskRecipient = rdWFInstanceTask.Recipients(0)

        Dim rdUser As RDUser = GetUser(srv, props.Item("Requester"))
        If rdUser Is Nothing Then
            Throw New Exception("User Cannot Found")
        End If
        rdWFInstanceTaskRecipient.ChangeUser(rdUser)
        wfi.ChangeOwner(rdUser)
        wfi.Update()
        srv.ExecuteNonQuery("Update WFInstanceTasks set initiator = " + rdUser.ID.ToString + " Where witId = " + rdWFInstanceTask.ID.ToString + "")
        Return rdWFInstanceTask.ID
    End Function

    Public Shared Function GetUser(ByRef srv As DMSServer, ByRef userName As String) As RDUser
        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        Dim rdCol As RDCollection = srv.GetDomainList()
        Dim domainIdList As New List(Of Long)
        domainIdList.Add(0)

        For Each rdLdap As RDLDAPConfig In rdCol
            domainIdList.Add(rdLdap.ID)
        Next

        For Each domainId As Long In domainIdList
            Dim rdUser As RDUser = uc.GetUserByName(userName, domainId)
            If Not rdUser Is Nothing Then
                Return rdUser
            End If
        Next
        Return Nothing
    End Function


End Class
