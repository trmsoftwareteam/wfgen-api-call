﻿Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml

Public Class LatestComment


    Implements AntelopeWebPlugin.IPluginCustomService

    Public ReadOnly Property FuncId As String Implements AntelopeWebPlugin.IPlugin.FuncId
        Get
            Return "LatestComment"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMS.CoreAPI.DMSServer, parentId As String, id As String, props As System.Collections.Generic.Dictionary(Of String, String)) As System.Xml.XmlNode Implements AntelopeWebPlugin.IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()
        Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
        xmlDocument.AppendChild(resultNode)
        Try
            Dim wiId As Long = CLng(props.Item("wiId"))
            Dim wc As RDWFInstanceController = srv.CreateController(ControllerType.RDWFInstanceController)
            Dim wfi As RDWFInstance = wc.GetWFInstanceById(wiId)
            Dim taskList As RDCollection = wfi.TasksList
            Dim allComments As RDCollection = wfi.GetAllComments()
            Dim lastComment As String = ""
            For Each rdTask As RDWFInstanceTask In taskList
                Dim rdWFInstanceTaskRecipient As RDWFInstanceTaskRecipient = rdTask.Recipients(0)
                Dim comments As String = rdWFInstanceTaskRecipient.Comments
                For Each c As RDComment In allComments
                    srv.NewException(LogManager.LogLevel.Exception, c.Parent.SecuType.ToString)
                    srv.NewException(LogManager.LogLevel.Exception, c.Parent.ID.ToString)
                    srv.NewException(LogManager.LogLevel.Exception, c.Parent.Name)
                    If c.RelatedId = rdWFInstanceTaskRecipient.ID Then
                        If comments <> "" Then
                            comments &= "<br/>"
                        End If
                        comments &= c.Comment
                        lastComment = c.Comment
                    End If
                Next
            Next
            wfi.DocProps("Latest Comment") = lastComment
            wfi.Update()
            resultNode.InnerText = "Success"
        Catch ex As Exception
            resultNode.InnerText = "Error:" + ex.Message
        End Try

        Return xmlDocument

    End Function
End Class
