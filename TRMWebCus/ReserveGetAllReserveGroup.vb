﻿Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml
Public Class ReserveGetAllReserveGroup
    Implements AntelopeWebPlugin.IPluginCustomService

    Dim approval_gourp_id = New Long() {3, 4, 5, 6, 7, 9, 10}


    Public ReadOnly Property FuncId As String Implements AntelopeWebPlugin.IPlugin.FuncId
        Get
            Return "ReserveGetAllReserveGroup"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMS.CoreAPI.DMSServer, parentId As String, id As String, props As System.Collections.Generic.Dictionary(Of String, String)) As System.Xml.XmlNode Implements AntelopeWebPlugin.IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()
        Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
        xmlDocument.AppendChild(resultNode)
        Try
            Return GetGroup(srv, props("level")).ToXml
        Catch ex As Exception

        End Try
        Return xmlDocument
    End Function

    Public Function GetGroup(ByRef srv As DMS.CoreAPI.DMSServer, ByRef level As String) As RDCollection
        Dim groupIdList As New List(Of Long)

        For index As Integer = CInt(level) To 7
            groupIdList.Add(approval_gourp_id(index - 1))
        Next




        Dim rdCollection As New RDCollection(srv)
        Dim uc As RDUserRoleController = srv.CreateController(ControllerType.RDUserRoleController)
        For Each groupId As Long In groupIdList
            Dim rdRole As RDRole = uc.GetRoleById(groupId)
            For Each rdUser As RDUser In rdRole.GetAllUsers(True)
                Dim isExist As Boolean = False

                For Each existUser As RDUser In rdCollection
                    If rdUser.ID = existUser.ID Then
                        isExist = True
                    End If
                Next

                If Not isExist Then
                    rdCollection.Add(rdUser)
                End If


            Next

        Next

        Return rdCollection
    End Function
End Class
