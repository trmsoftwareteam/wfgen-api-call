﻿Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml
Public Class StartTRMPaymentWorkflow
    Implements AntelopeWebPlugin.IPluginCustomService

    Public ReadOnly Property FuncId As String Implements AntelopeWebPlugin.IPlugin.FuncId
        Get
            Return "StartTRMPaymentWorkflow"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMS.CoreAPI.DMSServer, parentId As String, id As String, props As System.Collections.Generic.Dictionary(Of String, String)) As System.Xml.XmlNode Implements AntelopeWebPlugin.IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()

        Try
            Dim dc As RDDocumentController = srv.CreateController(ControllerType.RDDocumentController)
            Dim template As RDDocument = dc.GetDocumentById(CLng(srv.GetCustomSetting("TRMPaymentWorkflowId")))
            Dim witId As Long = GlobalFunction.StartWorkflow(srv, template, props)
            Dim url As String = srv.GetSystemSetting(ConfigController.SystemSetting.ServerUrl) + "WFTaskView.aspx?witId=" + witId.ToString
            Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
            xmlDocument.AppendChild(resultNode)
            resultNode.InnerText = url

        Catch ex As Exception
            Dim errorNode As XmlNode = xmlDocument.CreateElement("Error")
            xmlDocument.AppendChild(errorNode)
            errorNode.InnerText = ex.Message
        End Try
        Return xmlDocument
    End Function


End Class
