﻿Imports AntelopeWebPlugin
Imports DMS.CoreAPI
Imports GenericDatabase
Imports System.Xml
Public Class StartToHRWorkflow
    Implements AntelopeWebPlugin.IPluginCustomService

    Public ReadOnly Property FuncId As String Implements IPlugin.FuncId
        Get
            Return "StartToHRWorkflow"
        End Get
    End Property

    Public Function Custom(ByRef srv As DMSServer, parentId As String, id As String, props As Dictionary(Of String, String)) As XmlNode Implements IPluginCustomService.Custom
        Dim xmlDocument As New XmlDocument()

        Try
            Dim dc As RDDocumentController = srv.CreateController(ControllerType.RDDocumentController)
            Dim template As RDDocument = dc.GetDocumentById(CLng(20256))

            Dim witId As Long = GlobalFunction.StartWorkflow(srv, template, props)

            Dim url As String = srv.GetSystemSetting(ConfigController.SystemSetting.ServerUrl) + "WFTaskView.aspx?witId=" + witId.ToString
            Dim resultNode As XmlNode = xmlDocument.CreateElement("Result")
            xmlDocument.AppendChild(resultNode)
            resultNode.InnerText = url

        Catch ex As Exception
            Dim errorNode As XmlNode = xmlDocument.CreateElement("Error")
            xmlDocument.AppendChild(errorNode)
            errorNode.InnerText = ex.Message
        End Try
        Return xmlDocument
    End Function
End Class
