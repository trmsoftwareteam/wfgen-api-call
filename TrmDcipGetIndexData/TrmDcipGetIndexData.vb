﻿Imports Antelope.Imaging.Common
Imports System.IO
Public Class TrmDcipGetIndexData
    Implements IProcess
    Private _config As TrmDcipGetIndexDataConfig
    Public Sub CompleteProcess() Implements Antelope.Imaging.Common.IProcess.CompleteProcess

    End Sub

    Public Function GetProcessMode() As Antelope.Imaging.Common.ProcessMode Implements Antelope.Imaging.Common.IProcess.GetProcessMode
        Return 0
    End Function

    Public Sub LoadConfig(ByRef config As Antelope.Imaging.Common.IConfig) Implements Antelope.Imaging.Common.IProcess.LoadConfig
        If TypeOf (config) Is TrmDcipGetIndexDataConfig Then
            _config = config
        Else
            Throw New Exception("Invalid config exception : the input config is not csv config")
        End If
    End Sub

    Public Function Process(ByRef doclist As System.Collections.ArrayList) As String Implements Antelope.Imaging.Common.IProcess.Process
        Dim msg As String = ""
        For Each d As Document In doclist
            msg &= ProcessOne(d)
        Next
        Return msg
    End Function

    Public Function ProcessOne(ByRef doc As Antelope.Imaging.Common.Document) As String Implements Antelope.Imaging.Common.IProcess.ProcessOne
        Dim msg As String = ""
        Try
            Dim fileName As String = doc.Props("Filename")
            Dim fileDatas() As String = fileName.Split("-")
            If fileDatas.Length > 3 Then
                Dim keyword As String = fileDatas(3)
                Dim recordFromCsv As String = GetRecordByKeyword(keyword)
                If Not String.IsNullOrEmpty(recordFromCsv) Then
                    Dim recordArr() As String = recordFromCsv.Split(",")

                    Dim docType As String = recordArr(0)
                    Dim subDocType As String = recordArr(1)
                    Dim indexFieldNameList As New List(Of String)
                    For index As Integer = 3 To recordArr.Length - 1
                        indexFieldNameList.Add(recordArr(index))
                    Next

                    doc.AddProp("Document Type", docType)
                    doc.AddProp("Sub Document Type", subDocType)
                    For index As Integer = 4 To fileDatas.Length - 1
                        If Not String.IsNullOrEmpty(indexFieldNameList.Item(index - 4)) Then
                            doc.AddProp(indexFieldNameList.Item(index - 4), fileDatas(index))
                        End If
                    Next



                End If



            End If

        Catch ex As Exception

        End Try
        Return msg
    End Function

    Public Function GetRecordByKeyword(ByRef keyword As String) As String
        Dim objReader As New System.IO.StreamReader(_config.CSVPath)
        Do While objReader.Peek() <> -1
            Dim lineString As String = objReader.ReadLine()
            Dim lineValues() As String = lineString.Split(",")
            If lineValues.Length > 2 Then
                If lineValues(2) = keyword Then
                    Return lineString
                End If
            End If


        Loop
        objReader.Close()

        Return ""
    End Function

End Class
