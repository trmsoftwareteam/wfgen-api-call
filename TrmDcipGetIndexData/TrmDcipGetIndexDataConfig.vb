﻿Imports Antelope.Imaging.Common

Public Class TrmDcipGetIndexDataConfig
    Inherits ConfigBase
    Private Enum CP
        CsvPath
    End Enum

    Public Property CSVPath() As String
        Get
            Return GetConfig(CP.CsvPath)
        End Get
        Set(ByVal value As String)
            SetConfig(CP.CsvPath, value)
        End Set
    End Property


    Public Sub New()
        addProp(New ConfigProperty(CP.CsvPath, ConfigProperty.DataType.Text, "Csv Path", True), "")
    End Sub
End Class
